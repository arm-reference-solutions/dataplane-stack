#!/usr/bin/env bash

# Copyright (c) 2022-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

LIST_OF_APPS="sudo net-tools build-essential manpages-dev libnuma-dev python3
              python3-venv cmake meson pkg-config python3-pyelftools lshw
              util-linux iperf3 nginx libboost-all-dev ragel libsqlite3-dev
              libpcap-dev libdumbnet-dev libluajit-5.1-dev zlib1g-dev
              libhwloc-dev liblzma-dev libssl-dev libgoogle-perftools-dev
              libpcre++-dev flex openssl libunwind-dev wget"

echo "@@@@@@@@@@@@@@@@@@  Installing packages ...   @@@@@@@@@@@@@@@@@@@@"

if [[ -r /etc/os-release ]]; then
    OS_NAME=$(grep -w "NAME" /etc/os-release | cut -d= -f2 | tr -d '"')
    OS_VERSION_ID=$(grep -w "VERSION_ID" /etc/os-release | cut -d= -f2 | tr -d '"')

    if [[ "${OS_NAME}" == "Ubuntu" ]]; then
        echo "OS: ${OS_NAME} ${OS_VERSION_ID}"
    else
        echo "Error: OS: ${OS_NAME}"
        echo "Error: The current dataplane-stack is intended for Ubuntu"
        exit 1
    fi

    if [[ "${OS_VERSION_ID}" != "22.04" ]];then
        echo "Warning: OS: ${OS_NAME} ${OS_VERSION_ID}"
        echo "Warning: The current dataplane-stack has been validated on Ubuntu 22.04"
        echo "Warning: Ubuntu 22.04 is recommended"
    fi
else
    echo "Error: OS information detection failed"
    exit 1
fi

apt-get update
apt-get install -y $LIST_OF_APPS

echo "@@@@@@@@@@@@@@@@@@     Packages installed     @@@@@@@@@@@@@@@@@@@@"
echo "@ Remember to setup grub settings according to the documentation @"
