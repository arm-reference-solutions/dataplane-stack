..
  # Copyright (c) 2022-2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

###
FAQ
###

#. Q: Could you please provide the project description of functionality or
   purpose?

   A: The network functions this software provides serve multiple purposes
   to:

   #. Showcase the integration of the various components and act as proof of
      concept to all the stakeholders.

   #. Allow for performance analysis/optimization with a solution that is
      close to customers’ production deployment.

   #. Provide customers with an out-of-the-box reference solution for rapid
      design modeling.

#. Q: What are the Fixed Virtual Platforms and what are they used for?

   A: The Fixed Virtual Platforms (FVPs) are some pre-silicon
   simulation platforms, which could be used to develop and validate
   software for CPU features in the early stage. Running the software
   on these platforms enables functional verification of software stacks for
   pre-silicon platforms.

#. Q: Please explain the data plane and control plane, and their differences?

   A: The terms “control plane” and “data plane” are all about the separation of
   responsibilities within a networking system. The two most commonly referenced
   compositions in networking are the control plane and the data plane. The
   control plane is the part of a network that controls how data is forwarded,
   while the data plane is the actual forwarding process.

   * **Control plane**: refers to all of the functions and processes that
     determine which path to use to send the packets or frames. The control
     plane is responsible for populating the routing table, drawing the network
     topology, forwarding table and hence enabling the data plane functions.
     The control plane is the process of learning what will be done before
     sending the packets or frames.

   * **Data plane**: refers to all of the functions and processes that forward
     packets/frames from one interface to another based on control plane logic.
     Routing table, forwarding table and the routing logic constitute the data
     plane function. The data plane packets go through the router, and incoming
     and outgoing frames are regulated based on the control plane logic.
     The data plane is moving the actual packets based on what we learned from
     control plane.

#. Q: What's user space network software?

   A: User space network software takes exclusive control of a network adapter,
   implements the whole NIC driver and develops the packet processing framework
   completely in user space.

   There are several primary reasons to move the networking functionalities
   from the kernel to user space:

   * Reduce the number of context switches required to process packet data, as
     each syscall causes a context switch, which takes up time and resources.

   * Reduce the amount of software on the stack. The Linux kernel provides
     abstractions, which are designed for general purpose and could be quite
     complicated in implementing packet processing. Customized
     implementation could remove unnecessary abstractions, simplify the logic,
     and improve performance.

   * User space drivers are easier to develop and debug than kernel drivers.
     Developing networking functions and getting them merged in mainline kernel
     would take considerable time and effort. Moreover, the function release
     would be bound to Linux’s release schedule. Finally, bugs in the source
     code may cause the kernel to crash.

   The user space networking ecosystems have really matured ever since some
   user space networking projects became open source, e.g.
   `DPDK <https://www.dpdk.org/>`_,
   `VPP <https://fd.io/>`_,
   `Snort <https://www.snort.org/>`_.
   A whole ecosystem of technologies based on user space network
   software has emerged.

#. Q: How to install the Mellanox ConnectX OFED driver and update the NIC firmware?

   A: To use a Mellanox NIC, firstly install the OFED driver `MLNX_OFED <https://docs.nvidia.com/networking/display/mlnxofedv24010331/installing+mlnx_ofed>`_,
   and then `update the NIC Firmware <https://docs.nvidia.com/networking/display/mlnxofedv24010331/updating+firmware+after+installation>`_.

   The key steps are:

   * Download the `OFED driver <https://www.mellanox.com/page/mlnx_ofed_eula?mtag=linux_sw_drivers&mrequest=downloads&mtype=ofed&mver=MLNX_OFED-24.01-0.3.3.1&mname=MLNX_OFED_LINUX-24.01-0.3.3.1-ubuntu22.04-aarch64.iso>`_

   * Install the OFED driver:

   .. code-block:: shell

        sudo mkdir -p /mnt/ofed-driver # create mount folder
        sudo mount -o ro,loop MLNX_OFED_LINUX-24.01-0.3.3.1-ubuntu22.04-aarch64.iso /mnt/ofed-driver
        sudo /mnt/ofed-driver/mlnxofedinstall --upstream-libs --dpdk
        sudo umount /mnt/ofed-driver
        sudo rmdir /mnt/ofed-driver # clean-up mount folder

   * Install the Mellanox Firmware Tools (MFT):

   .. code-block:: shell

        wget https://www.mellanox.com/downloads/MFT/mft-4.27.0-83-arm64-deb.tgz
        tar xvf mft-4.27.0-83-arm64-deb.tgz
        cd mft-4.27.0-83-arm64-deb/
        sudo ./install.sh
        sudo mst start

   * Install the latest firmware for the NIC:

   .. code-block:: shell

        sudo mlxfwmanager --online -u -d <device PCIe address>

   * Alternatively, download a specific firmware version from `Mellanox Firmware Downloads <https://network.nvidia.com/support/firmware/firmware-downloads/>`_.

    To find the proper firmware to download for the NIC, first obtain its OPN and PSID via ``sudo mlxfwmanager -d <device PCIe address> --query``. The output will look similar to:

    .. code-block:: none

        Querying Mellanox devices firmware ...

        Device #1:
        ----------

          Device Type:      ConnectX7
          Part Number:      MCX755106AS-HEA_Ax
          Description:      NVIDIA ConnectX-7 HHHL Adapter Card; 200GbE (default mode) / NDR200 IB; Dual-port QSFP112; PCIe 5.0 x16 with x16 PCIe extension option; Crypto Disabled; Secure Boot Enabled
          PSID:             MT_0000000834
          PCI Device Name:  0000:01:00.0

    The PSID is listed in the output. The OPN will resemble the part number, but may not match exactly. As an example, the OPN for the sample output is MCX516A-CDA.
    After downloading the desired firmware, flash that firmware to the NIC:

    .. code-block:: shell

        sudo mlxfwmanager -d <device PCIe address> -i <path to unzipped FW download>

