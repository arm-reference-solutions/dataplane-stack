..
  # Copyright (c) 2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

############
VPP firewall
############

************
Introduction
************

Snort is an Intrusion Detection/Prevention System (IDS/IPS). Snort uses a
series of rules that help define malicious network activity. Packets matching
against these rules generate alerts for the user. You can use community curated
rules and you can define your own to either replace of supplement them. Beyond
alerts and logging, other actions may be defined, like dropping packets.

Vectorscan is a fork of Hyperscan that runs on Arm and other architectures. It's
a regular expression matching library. As part of deep packet inspection
performed by Snort, contents of the packets can be matched against regular
expression patterns. The matching engine built into Snort is slow. Vectorscan
works as a plugin that replaces the built-in search engine and provides a
significant performance increase thanks to use of vector extensions and other
optimizations.

Through use of the Snort plugin in VPP, Snort can be added to the VPP through
two new graph nodes: snort-enq and snort-deq. These allow packets to pass
through Snort, letting Snort perform its actions like dropping packets.

This guide explains how to create a firewall system that can reject/alert on
receiving malicious packets. The scripts provided will create a set of VPP and
Snort instances all connected together demonstrating its operation.

The demonstration can be run with a traffic generator creating synthetic
traffic or playing back already captured packets. A good packet capture to use
is one containing malicious traffic.

The Malware Capture Facility Project started at the CTU University is a good
source of malicious packet captures. Their samples are public and free to use
under [CC-BY](https://creativecommons.org/licenses/by/2.0/) license.

A good sample of small size to try is:
https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-42/botnet-capture-20110810-neris.pcap

Snort requires rules to perform meaningful work. For the case of synthetic
traffic, an example rules file with a single rule matching the generated traffic
has been prepared. If a packet capture is provided, the community rules are used
instead. These offer a more realistic workload.

The packet generator used is a VPP based traffic generator that will communicate
with the firewall via shared memory packet interface (memif). In this case, the
VPP packet generator instance is running on the same host as the firewall.
Alternatively, you can run the VPP firewall example using physical interfaces
to accept external traffic.

****************
Memif Connection
****************

Shared memory packet interface (memif) is software emulated Ethernet interface,
which provides high performance packet transmit and receive between VPP and user
application or between multiple VPP instances.

In this setup, we have one single memif interface that is configured to connect
the VPP based firewall and VPP based traffic generator. On the firewall side,
DPDK zero-copy memif interfaces are used. On VPP traffic generator side, VPP's
native memif interfaces are used for performance reasons.

.. figure:: ../images/firewall_memif.png
   :align: center
   :width: 500

   Memif connection

The user can quickly run VPP firewall over memif connection using the scripts
located at: ``$NW_DS_WORKSPACE/dataplane-stack/usecase/firewall``.

Overview
========

In memif connection scenario, the main operations of each script are as follows:

``run_vpp_tg.sh``

* Runs a VPP instance for the traffic generator on the specified CPU cores
* Creates two VPP memif interfaces in server role
* Configures a software traffic generator
* Starts the traffic generator

``run_vpp_fw.sh``

* Runs a VPP instance for the firewall on the specified CPU cores
* Creates two DPDK memif interfaces in client role with zero-copy enabled
* Brings interfaces up and set interfaces IP addresses
* Adds IP route entry making the second memif interface the gateway
* Adds Snort interfaces
* Launches Snort connected to the VPP instance

``traffic_monitor.sh``

* Monitors VPP interface counters and firewall throughput
  with VPP ``show interface`` and ``show runtime`` commands

``stop.sh``

* Stops VPP firewall and traffic generator instances and the Snort instance

For detailed usage on VPP CLI commands used in scripts, refer to the following
links:

- `VPP configuration dpdk section reference`_
- `VPP set interface ip address reference`_
- `VPP ip neighbor cli reference`_
- `VPP set interface state reference`_
- `VPP Snort plugin reference`_

Execution
=========

.. note::
        This execution requires at least three isolated cores for VPP workers.
        Cores 2-4 are assumed to be isolated in this guide.

Quickly set up VPP firewall/traffic generator instances with synthetic traffic.

.. code-block:: shell

        cd $NW_DS_WORKSPACE/dataplane-stack
        ./usecase/firewall/run_vpp_tg.sh -c 1,2,3
        ./usecase/firewall/run_vpp_fw.sh -c 1,4

.. note::
        - Use ``-h`` to check scripts supported options.

Examine VPP firewall's interfaces rx/tx counters and packet processing runtime:

.. code-block:: shell

        ./usecase/firewall/traffic_monitor.sh

Below is key output:

.. code-block:: none

          Name      Idx   State   MTU (L3/IP4/IP6/MPLS)   Counter       Count
        Ethernet0    1     up           9000/0/0/0       rx packets      4052
                                                         rx bytes      259328
                                                         drops              4
                                                         ip4             4052
        Ethernet1    2     up           9000/0/0/0       tx packets      4049
                                                         tx bytes      259136
        local0       0    down           0/0/0/0

.. code-block:: none

        Thread 1 vpp_wk_0 (lcore 4)
        Time 4.0, 10 sec internal node vector rate 1.00 loops/sec 12090235.99
        vector rates in 3.0021e3, out 1.0000e3, drop 9.8842e-1, punt 0.0000e0
        Name                State        Calls Vectors Suspends Clocks Vectors/Call
        Ethernet1-output    active        4047    4047        0 6.49e0         1.00
        Ethernet1-tx        active        4047    4047        0 5.79e0         1.00
        dpdk-input          polling   49605843    4051        0 1.46e4         0.00
        drop                active           4       4        0 7.10e1         1.00
        error-drop          active           4       4        0 4.33e1         1.00
        ethernet-input      active        4051    4051        0 9.34e0         1.00
        interface-output    active        4047    4047        0 3.84e0         1.00
        ip4-input           active        4051    4051        0 5.16e0         1.00
        ip4-lookup          active        4047    4047        0 3.79e0         1.00
        ip4-rewrite         active        4047    4047        0 5.73e0         1.00
        snort-deq        interrupt wa     8094    8098        0 9.27e0         1.00
        snort-enq           active        8098    8098        0 4.52e1         1.00
        unix-epoll-input    polling      48396       0        0 1.49e1         0.00

.. note::
        - VPP ``Ethernet0`` is the aliased name of the input memif interface in the example.
        - VPP ``Ethernet1`` is the aliased name of the output memif interface in the example.
        - ``vector rates`` provide insights into the packet processing throughput of a specific node or function in VPP.
        - ``Vectors/Call`` measures packet processing efficiency in VPP as operations per function call for a specific node or function.
        - ``snort-enq`` and ``snort-deq`` are graph nodes responsible for communicating with the Snort instance. Dropped packets will not be dequeued.

Snort is outputting its alerts to a log directory in the root of the dataplane
stack workspace. You can observe the alerts as they appear.

.. code-block:: shell

        sudo tail -f $NW_DS_WORKSPACE/alert_csv.txt

Stop all instances:

.. code-block:: shell

        ./usecase/firewall/stop.sh

If you try and launch a new instance while the old one is still running,
the old one will be stopped first. To allow multiple instances to coexist on one
machine, a suffix is used to avoid clashes. By default this suffix will be your
username but you can specify it manually using the ``-s`` option that all
scripts accept.

Using a recorded packet capture
===============================

If the suggested
`packet capture file <https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-42/botnet-capture-20110810-neris.pcap>_`
file is downloaded, it can be used with the traffic generator by passing its
filename to the script.


.. code-block:: shell

        ./usecase/firewall/run_vpp_tg.sh -c 1,2,3 -f botnet-capture-20110810-neris.pcap

Launching the firewall now will enable Snort community rules (if no rules are
specified using the ``-r`` option). You can read more about the rules on the
`Snort website <https://www.snort.org/faq/what-are-community-rules>`_.

Snort community rules are downloaded automatically if needed. You can also
trigger the download manually using the provided script.

.. code-block:: shell

        ./usecase/firewall/get_community_rules.sh

Snort community rules will be used by default unless the traffic generator
is using synthetic traffic in which case the ``example.rules`` file will be
used that matches some of the synthetic traffic generated. Custom rules can be
used by passing in the rules filename using the ``-r`` option of the firewall
script.

*******************
Ethernet Connection
*******************

In this scenario the firewall is running on machine connected to physical
interfaces and is connected with Ethernet adapters and cables. This way you can
use a separate machine as the software-based traffic generator, e.g.,
VPP/TRex/TrafficGen, or a hardware traffic generator,
e.g., IXIA/Spirent Smartbits.

.. figure:: ../images/firewall_nic.png
   :align: center
   :width: 500

   Ethernet connection

Find out which DUT interfaces are connected to the traffic generator.
``sudo ethtool --identify <interface_name>`` will typically blink a light on the
NIC to help identify the physical port associated with the interface.

Get interface names and PCIe addresses from ``lshw`` command:

.. code-block:: shell

        sudo lshw -c net -businfo

The output will look similar to:

.. code-block:: none

        Bus info          Device      Class      Description
        ====================================================
        pci@0000:07:00.0  eth0        network    RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
        pci@0001:01:00.0  enP1p1s0f0  network    MT27800 Family [ConnectX-5]
        pci@0001:01:00.1  enP1p1s0f1  network    MT27800 Family [ConnectX-5]

Of the two interfaces connected to the traffic generator, arbitrarily choose one
to be the input interface and the other to be the output interface. In this
setup example, ``enP1p1s0f0`` at PCIe address ``0001:01:00.0`` is the input
interface, and ``enP1p1s0f1`` at PCIe address ``0001:01:00.1`` is the output
interface.

Get the MAC address of the input interface ``enP1p1s0f0`` connected to the
traffic generator via ``ip link show enP1p1s0f0``. This MAC address will be used
as destination MAC address of generated traffic. Below is a sample output:

.. code-block:: none

        1: enP1p1s0f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8996 qdisc mq state UP mode DEFAULT group default qlen 1000
            link/ether b8:ce:f6:10:e4:6c brd ff:ff:ff:ff:ff:ff

The user can quickly run firewall over NIC connection using the scripts located
at: ``$NW_DS_WORKSPACE/dataplane-stack/usecase/firewall``.

Overview
========

In Ethernet connection scenario, the main operations of scripts are as follows:

``run_vpp_fw.sh``

* Runs a VPP instance for the firewall on the specified CPU cores
* Attaches two DPDK interfaces on the specified PCIe addresses to VPP
* Brings the interfaces up and set their IP addresses
* Adds IP route entries making memif2 the gateway
* Adds Snort interfaces
* Launches Snort connected to the VPP instance

``traffic_monitor.sh``

* Monitors VPP interface counters and firewall throughput
  via VPP ``show interface`` and ``show runtime`` commands

``stop.sh``

* Stops VPP firewall instance

Execution
=========

.. note::
        This execution requires at least one isolated core for VPP worker. Core
        2 is assumed to be isolated in this guide.

Quickly set up VPP firewall with input/output interface PCIe addresses on
specified cores for 1 flow:

.. code-block:: shell

        cd $NW_DS_WORKSPACE/dataplane-stack
        ./usecase/firewall/run_vpp_fw.sh -p 0001:01:00.0,0001:01:00.1 -c 1,2

.. note::
        Replace sample addresses in above command with desired PCIe addresses on
        DUT.

Send packets using the traffic generator on the other machine connected to the
first NIC. The packets can come from a pcap suggested in the introduction of
this guide or any of the many pcaps available publicly. A good source of these
is the `NETRESEC website <https://www.netresec.com/?page=PcapFiles>`_.

VPP firewall will forward all packets accepted by Snort out onto VPP output
interface on the second NIC.

Examine VPP firewall's interfaces rx/tx counters and packet processing runtime:

.. code-block:: shell

        ./usecase/firewall/traffic_monitor.sh

Here is key output:

.. code-block:: none

          Name      Idx   State   MTU (L3/IP4/IP6/MPLS)   Counter       Count
        Ethernet0    1     up           9000/0/0/0       rx packets      4052
                                                         rx bytes      259328
                                                         drops              4
                                                         ip4             4052
        Ethernet1    2     up           9000/0/0/0       tx packets      4049
                                                         tx bytes      259136
        local0       0    down           0/0/0/0

.. code-block:: none

        Thread 1 vpp_wk_0 (lcore 2)
        Time 4.0, 10 sec internal node vector rate 1.00 loops/sec 12090235.99
        vector rates in 3.0021e3, out 1.0000e3, drop 9.8842e-1, punt 0.0000e0
        Name                State        Calls Vectors Suspends Clocks Vectors/Call
        Ethernet1-output    active        4047    4047        0 6.49e0         1.00
        Ethernet1-tx        active        4047    4047        0 5.79e0         1.00
        dpdk-input          polling   49605843    4051        0 1.46e4         0.00
        drop                active           4       4        0 7.10e1         1.00
        error-drop          active           4       4        0 4.33e1         1.00
        ethernet-input      active        4051    4051        0 9.34e0         1.00
        interface-output    active        4047    4047        0 3.84e0         1.00
        ip4-input           active        4051    4051        0 5.16e0         1.00
        ip4-lookup          active        4047    4047        0 3.79e0         1.00
        ip4-rewrite         active        4047    4047        0 5.73e0         1.00
        snort-deq        interrupt wa     8094    8098        0 9.27e0         1.00
        snort-enq           active        8098    8098        0 4.52e1         1.00
        unix-epoll-input    polling      48396       0        0 1.49e1         0.00

.. note::
        - VPP ``Ethernet0`` is the aliased name of the input interface, which is at PCIe address ``0001:01:00.0`` in the example.
        - VPP ``Ethernet1`` is the aliased name of the output interface, which is at PCIe address ``0001:01:00.1`` in the example.

Stop VPP firewall:

.. code-block:: shell

        ./usecase/firewall/stop.sh


*********
Resources
*********

#. `VPP configuration dpdk section reference <https://s3-docs.fd.io/vpp/23.10/configuration/reference.html#the-dpdk-section>`_
#. `VPP configuration reference <https://s3-docs.fd.io/vpp/23.10/configuration/reference.html>`_
#. `VPP set interface ip address reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet_ip.html#set-interface-ip-address>`_
#. `VPP ip neighbor cli reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet_ip-neighbor.html>`_
#. `VPP set interface state reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet.html#set-interface-state>`_
#. `VPP ip route reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet_ip.html#ip-route>`_
#. `VPP cli reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/index.html>`_
#. `VPP Snort plugin reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_plugins_snort.html>`_
