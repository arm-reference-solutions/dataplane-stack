..
  # Copyright (c) 2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

#################################
VPP Stateful Access Control Lists
#################################

************
Introduction
************

VPP access control list (ACL) are used as a security feature for blocking or permitting traffic based on L2-L4 layers.
The ACL use case extends the :doc:`L3 forwarding application <l3_forwarding_vpp>`. by adding ACL rules. Once the application
receives a packet, it checks whether there is any ACL permit/deny rule, and makes
the decision of forwarding the packet accordingly.

ACL rules are applied to accept or deny a packet based on layer 2 to layer 4
protocols. In L3-L4 ACLs, IP address mask and layer 4 protocols of the packets are checked, to apply
permit and deny rules. For L2-L3 ACLs, source MAC addresses of the packets are checked and permit and deny rules apply on specific
MAC addresses along with L3 addresses. If the rules permit packet to transfer, packet is forwarded by using L3
forwarding table.

This guide explains how to use the VPP based ACL rules using memif interfaces.
Users can execute bundled scripts in dataplane-stack repo to quickly establish the ACL rules on L3 forwarding cases.

Shared memory packet interface (memif) is software emulated Ethernet interface,
which provides high performance packet transmit and receive between VPP and user
application or multiple VPP instances.

In this setup, two pairs of memif interfaces are configured to connect VPP L3 router
instance and VPP based traffic generator. ACL rules are applied on L3 router. On
VPP router side, DPDK zero-copy memif interfaces are used for testing VPP + DPDK stack.
On VPP traffic generator side, VPP's native memif interfaces are used for performance reason.

.. figure:: ../images/ACL.png
  :align: center
  :width: 500

  Memif connection


The user can quickly run VPP ACL applications over memif connection using the scripts
located at: ``$NW_DS_WORKSPACE/dataplane-stack/usecase/l3-l4_acl`` and ``$NW_DS_WORKSPACE/dataplane-stack/usecase/l2-l3_acl``.

Overview
========

In memif connection scenario, the main operations of each script are as follows. These files have same filename for both l3-l4 ACL folder and l2-l3 ACL folder.


``vpp_tg_start.sh``

* Run a VPP instance for the traffic generator on the specified CPU cores
* Create two VPP memif interfaces in server role
* Configure a software traffic generator
* Create traffic generator streams matching rules that will be added in ``config_vpp_acl_rules.sh``, and disable all streams

``vpp_acl_start.sh``

* Run a VPP instance for the router on the specified CPU cores
* Create two DPDK memif interfaces in client role with zero-copy enabled
* Bring interfaces up and set interfaces IP addresses
* Add IP route entry in the L3 forwarding table
* Add ACL rules and attach these rules to the memif interfaces

``vpp_traffic_send.sh``

* Enable traffic stream matching per traffic rule and monitor the traffic using ``show interface`` and ``show trace`` commands
* ``show trace`` command shows which ACL policy is followed for permitting or dropping the packets

``stop_vpp_tg_acl.sh``

* Stop both VPP router and traffic generator instances

For detailed usage on VPP CLI commands used in scripts, refer to the following links and files:

- ``0001-l2_acl_cli.patch``: This patchset is added for L2 ACL CLI commands, in folder ``$NW_DS_WORKSPACE/dataplane-stack/patches``. These commands are not supported in VPP version 23.10. 
- `VPP configuration dpdk section reference`_.
- `VPP set interface ip address reference`_
- `VPP ip neighbor cli reference`_
- `VPP set interface state reference`_
- `VPP ACL cli`_.
- `VPP ACL examples`_

Execution
=========

.. note::
        This execution requires at least three isolated cores for VPP workers. Cores 2-4
        are assumed to be isolated in this guide.

Set up VPP router/traffic generator and test ACL and L3 forwarding use case with 60 byte packets:

.. code-block:: shell

        cd $NW_DS_WORKSPACE/dataplane-stack
        ./usecase/<acl_folder>/config_vpp_tg_acl.sh -c 1,2,4 -l 60
        ./usecase/<acl_folder>/config_vpp_acl_rules.sh -c 1,3
        ./usecase/<acl_folder>/start_vpp_tg_acl.sh

.. note::
        - Use ``-h`` to check scripts supported options.
        - Larger packets may reduce VPP traffic generator performance. Changing the
          packet length via ``-l`` option should not affect VPP router performance.

Following is the output example of permit and deny ACL rules. Snapshot of output of some ACL rules is as following:


.. tabs::
        .. tab:: L2-L3 ACLs
                .. code-block:: shell

                        ./usecase/l2-l3_acl/start_vpp_tg_acl.sh


                .. code-block:: none

                        ...
                        RULE 4 :
                        deny ip 0.0.0.0/0 mac 00:00:00:00:00:00 mask 00:00:00:00:00:00
                                Name               Idx    State  MTU (L3/IP4/IP6/MPLS)       Counter          Count
                        Ethernet0                         1      up          9000/0/0/0     rx packets        101632
                                                                                            rx bytes          6097920
                                                                                            drops             101632
                                                                                            ip4               101888
                        Ethernet1                         2      up          9000/0/0/0
                        local0                            0     down          0/0/0/0
                        INACL: sw_if_index 1, next_index 0, table_index 11, offset 1216
                        ip4-input: input ACL table-miss drops
                        ...

                .. code-block:: none

                        ...
                        RULE 1 :
                        permit ip 10.0.0.0/8 mac 08:22:48:66:54:64 mask ff:ff:ff:ff:ff:ff
                                Name               Idx    State  MTU (L3/IP4/IP6/MPLS)        Counter          Count
                        Ethernet0                         1      up          9000/0/0/0     rx packets         4658944
                                                                                            rx bytes           279536640
                                                                                            ip4                4658944
                        Ethernet1                         2      up          9000/0/0/0     tx packets         4658944
                                                                                            tx bytes           279536640
                        local0                            0     down          0/0/0/0
                        INACL: sw_if_index 1, next_index 1, table_index 17, offset 1408
                        ...


        .. tab:: L3-L4 ACLs
                .. code-block:: shell

                        ./usecase/l3-l4_acl/start_vpp_tg_acl.sh

                .. code-block:: none

                        ...
                        RULE 10 : Deny all
                        deny src 0.0.0.0/0 dst 0.0.0.0/0
                                Name               Idx    State  MTU (L3/IP4/IP6/MPLS)     Counter          Count
                        Ethernet0                   1      up          9000/0/0/0         rx packets        156160
                                                                                          rx bytes          9369600
                                                                                          drops             156160
                                                                                          ip4               156416
                        Ethernet1                   2      up          9000/0/0/0
                        local0                      0     down          0/0/0/0
                        00:00:26:347126: acl-plugin-in-ip4-fa
                        acl-plugin: lc_index: 0, sw_if_index 1, next index 0, action: 0, match: acl 10 rule 0 trace_bits 00000000
                        acl-plugin-in-ip4-fa: ACL deny packets
                        ...

                .. code-block:: none

                        ...
                        RULE 9 :
                        permit+reflect src 30.10.10.0/24 dst 30.120.10.0/24 UDP
                                Name               Idx    State  MTU (L3/IP4/IP6/MPLS)       Counter          Count
                        Ethernet0                   1      up          9000/0/0/0          rx packets         492800
                                                                                           rx bytes           29568000
                                                                                           ip4                492800
                        Ethernet1                   2      up          9000/0/0/0          tx packets         492800
                                                                                           tx bytes           29568000
                        local0                      0     down          0/0/0/0

                        00:00:29:738866: acl-plugin-in-ip4-fa
                        acl-plugin: lc_index: -1, sw_if_index 1, next index 1, action: 3, match: acl -1 rule 0 trace_bits 80000000
                        ...

.. note::
        - VPP ``Ethernet0`` is the aliased name of the input memif interface in the example.
        - VPP ``Ethernet1`` is the aliased name of the output memif interface in the example.
        - VPP ``show trace`` shows ``INACL`` for L2-L3 ACL and ``acl-plugin-in-ip4-fa`` for L3-L4 ACL. It shows which rule was followed and why packet was forwarded or dropped.

Stop both VPP router and traffic generator instances:


.. code-block:: shell

        ./usecase/<acl_folder>/stop.sh

*********
Resources
*********

#. `VPP configuration dpdk section reference <https://s3-docs.fd.io/vpp/23.10/configuration/reference.html#the-dpdk-section>`_
#. `VPP configuration reference <https://s3-docs.fd.io/vpp/23.10/configuration/reference.html>`_
#. `VPP set interface ip address reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet_ip.html#set-interface-ip-address>`_
#. `VPP ip neighbor cli reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet_ip-neighbor.html>`_
#. `VPP set interface state reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet.html#set-interface-state>`_
#. `VPP ip route reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/clis/clicmd_src_vnet_ip.html#ip-route>`_
#. `VPP cli reference <https://s3-docs.fd.io/vpp/23.10/cli-reference/index.html>`_
#. `VPP ACL cli <https://s3-docs.fd.io/vpp/22.02/cli-reference/clis/clicmd_src_plugins_acl.html>`_
#. `VPP L2 ACL examples <https://wiki.fd.io/view/VPP/SecurityGroups>`_
#. `VPP ACL examples <https://fd.io/docs/vpp/v2009/usecases/acls.html>`_
#. `L3 forwarding application <https://dataplane-stack.docs.arm.com/en/nw-ds-2023.05.31/user_guide/ipv4_vpp.html>`_
