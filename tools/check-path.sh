#!/usr/bin/env bash

# Copyright (c) 2022-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

check_binary()
{
    echo "Checking ${2} binary path..."
    binary=$(command -v "${3}")

    if ! [[ "${binary}" ]]; then
          echo
          echo "Can't find ${2} at: ${3}"
          echo
          exit 1
    fi

    echo "Found ${2} binary at: ${binary}"
    eval "${1}=\$binary"
}

check_vpp()
{
    check_binary vpp_binary "VPP" "${DATAPLANE_TOP}/components/vpp/build-root/install-vpp-native/vpp/bin/vpp"
}

check_vppctl()
{
    check_binary vppctl_binary "VPPCTL" "${DATAPLANE_TOP}/components/vpp/build-root/install-vpp-native/vpp/bin/vppctl"
}

check_snort()
{
    check_binary snort_binary "Snort" "${DATAPLANE_TOP}/components/snort3/install/bin/snort"
}

check_ldp()
{
    echo "Checking libvcl_ldpreload.so path..."
    LDP_PATH="${DATAPLANE_TOP}/components/vpp/build-root/install-vpp-native/vpp/lib/aarch64-linux-gnu/libvcl_ldpreload.so"

    if ! [[ -e ${LDP_PATH} ]]; then
          echo
          echo "Can't find libvcl_ldpreload.so at: ${LDP_PATH}"
          echo
          exit 1
    fi

    echo "Found libvcl_ldpreload.so at: ${LDP_PATH}"
}


cal_cores()
{
  IFS=',' read -ra array <<< "$1"

  count=0

  for item in "${array[@]}"; do
      if [[ $item == *-* ]]; then
          start=${item%-*}
          end=${item#*-}
          count=$((count + end - start + 1))
      else
          count=$((count + 1))
      fi
  done

  echo ${count}
}