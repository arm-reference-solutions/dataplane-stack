# Dataplane Stack

Copyright (c) 2022-2024, Arm Limited. All rights reserved.

## Introduction

Dataplane Stack aims to provide a high-throughput packet processing software stack and solutions to solve customer and partner use cases in networking applications. It is implemented, validated and deployed on the Arm AArch64 architecture.

The Dataplane Stack architecture is shown in the following figure:

![Dataplane Stack solution architecture](doc/images/dataplane-stack-architecture.png "Dataplane Stack solution architecture")

The Dataplane Stack solution serves multiple purposes:
* Showcase the integration of various networking components and act as proof of concept
* Allow for performance analysis/optimization with a solution that is close to real world use cases
* Provide users with an out-of-the-box reference solution for rapid development

The targeted and verified platforms are:
* Ampere Altra (Neoverse-N1) - Functionalities and Performance

## Important Notes

### Safety and Security

Arm Neoverse reference design software solutions are example software projects containing downstream versions of open source components. Although the components in these solutions track their upstream versions, users of these solutions are responsible for ensuring that, if necessary, these components are updated before use to ensure they contain any new functional or security fixes that may be required

# Getting Started

Please refer to the [Quickstart Guide](./doc/quickstart.rst) to get started using this solution. The same Quickstart Guide and complete documentation generated from this repository are also available in the [Dataplane Stack online documentation](https://dataplane-stack.docs.arm.com).

# Limitations and Known Issues

* Users of this software stack must consider safety and security implications according to their own usage goals
* Does not provide a native traffic generator


# Feedback and Support

To provide feedback or to request support, please contact the project maintainers
by email at dataplane-stack@arm.com, or check the 'MAINTAINERS' file
in the root of the repository for module/feature/use-case specific maintainers.

Arm licensees may also contact Arm via their partner managers to request support.
