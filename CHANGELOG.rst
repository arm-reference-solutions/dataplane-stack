..
  # Copyright (c) 2022-2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

#########################
Changelog & Release Notes
#########################

**********************
Dataplane Stack v24.03
**********************

New Features
============

  * IPSec use case with VPP
  * L3 forwarding over memif interfaces with VPP + DPDK

Changed
=======

Main software components versions:

  * DPDK v23.07
  * VPP v23.10

Tested platforms Ubuntu version:

  * Ubuntu 22.04

Use case changes:

  * Utilize zero-copy DPDK memif interfaces for L3 forwarding and L2 switching.

Documentation:

  * Guide users on correct grub settings.
  * Update OFED driver version.

Tools:

  * Enhance commit message check for correct tag order.
  * Do not modify grub settings in ``setup.sh``.

Limitations
===========

  * Arm Neoverse reference design software solutions are example software projects containing downstream versions of open source components. Although the components in these solutions track their upstream versions, users of these solutions are responsible for ensuring that, if necessary, these components are updated before use to ensure they contain any new functional or security fixes that may be required.
  * Does not provide native traffic generator for all use cases.

Resolved Issues
===============

None.

Known Issues
============

None.

**********************
Dataplane Stack v23.05
**********************

New Features
============

  * Integrated and enabled L2 switching with VPP
  * Integrated and enabled TCP termination with VPP
  * Integrated and enabled SSL reverse proxy with VPP

Changed
=======

Main software components versions:

  * DPDK v22.11
  * VPP v23.02

Documentation:

  * Added user guides for L2 switching, TCP termination and SSL reverse proxy.
  * Replaced RDMA driver with DPDK driver in quickstart guide and VPP IPv4 L3fwd user guide.
  * Improved code block display in quickstart guide, DPDK L3fwd and VPP IPv4 L3fwd user guides.
  * Updated required CPU cores number and isolated cores in guides.
  * Added instructions to install specific Mellanox ConnectX-5 firmware in FAQ.

Use case scripts:

  * Added scripts to run L2 switching, TCP termination and SSL reverse proxy use cases.

Tools:

  * Enabled Sphinx based spell check.
  * Imported traffic generator tool wrk2.
  * Improved license title year check.

Limitations
===========

  * Users of this software stack must consider safety and security implications according to their own usage goals.
  * Does not provide native traffic generator for all use cases.

Resolved Issues
===============

None.

Known Issues
============

None.

**********************
Dataplane Stack v22.06
**********************

New Features
============

  * Integrated and enabled DPDK L3Fwd sample application
  * Integrated and enabled IPv4 forwarding with VPP

Changed
=======

Initial version.

Tested platforms:

  * DUT

    * Ampere Altra (Neoverse-N1)

      * Ubuntu 20.04.3 LTS (Focal Fossa)
      * Kernel 5.17.0-051700-generic

  * NIC

    * Mellanox ConnectX-5

      * OFED driver: MLNX_OFED_LINUX-5.4-3.1.0.0
      * Firmware Version 16.30.1004 (MT_0000000013)

Main software components versions:

  * DPDK v21.11
  * VPP v22.02

Documentation:

  * Initial documentation with README, overview, quickstart guide, user guide and FAQ.

Tools:

  * Initialized QA checks to validate:

    * Sphinx based documentation generation
    * Dependent component compilation
    * Commit log format check
    * License-Identifier check

Limitations
===========

  * Users of this software stack must consider safety and security implications according to their own usage goals.
  * Does not provide native traffic generator.

Resolved Issues
===============

None.

Known Issues
============

None.

