# Contributing to Arm Dataplane-Stack Reference Solution
All contributions to this project happens through GitLab. The contribution
can be:
1. Fixing defects in the software
2. Adding features to the software
3. Fixing errors in documentation
4. Expanding or adding to the documentation

Note that, the reporting of issues is also through GitLab.

## Access Permissions
To contribute to this project via GitLab, one needs to have a GitHub account.
The GitHub account credentials are only used for athentication and
authorization. The project software and all contributions to it, remain in
GitLab.

In addition to above, one needs permission to create fork of this repository.
For this, place your request for permission at:
https://gitlab.arm.com/documentation/contributions

## Commit Requirements
Each commit must follow the following guidelines.
- Make sure that the copyright notice at the top in the added or modified
  files is up-to-date with correct year as shown in below example:
  # Copyright (c) 2022-2024, Arm Limited.
- In new files added, make sure that the above copyright notice is followed by
  SPDX tag for Apache 2.0 license as below:
  # SPDX-License-Identifier: Apache-2.0
- The commit message should have the summary line and the body explaining
  the change.
- Finally, the commit must be signed-off by the author (using git commit
  --signoff or -s option)

## Steps To Contribute
The steps to contribute are in line with standard GitLab work flow. That is,
all changes are submitted via merge requests (MR). The MR will be reviewed
and approved. Once the changes are approved, the maintainer would merge the
contribution in the repository. The various steps in this process are as below.

- Fork this project repository and create a topic branch off the "main" branch.
- Add or make changes in that topic branch on the forked repository.
- Add new use case application, if needed, to showcase the additions.
- Update the documentation where required.
- Maintain consistency with existing software and documentation content.
- Run the exsiting use cases (and any new use case app) to ensure nothing is
  broken.
- Submit the Merge Request of your topic branch.
- Once the MR is approved and merged in the repository, resync your forked
  repository.
- After resyncing your fork, the topic branch can be deleted in your forked
  repository.

## Defect Reporting
When reporting defects, please provide all the information about the
test case such as:
- Hardware information including details about CPU, Memory, NICs, etc.
- System configuration such as OS version, kernel version, BIOS/Boot
  settings, huge page settings, etc.
- Tools such as compiler version along with their command line parameters.
- Use case topology and testing methodology to reproduce the defect.

## License
All contributions made to this project will be deemed under Apache 2.0 License.
It is the responsibility of the contributor to make sure that all contributions
are original and do not infringe upon other copyrights. The release of this
project happens under same Apache 2.0 License.
