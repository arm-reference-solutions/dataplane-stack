# Copyright (c) 2022-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

VENV_DIR      ?= doc/venv
PYTHON        ?= python3
NUM_JOBS      ?= $(shell nproc)
ROOT_DIR      := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: all
all: dpdk vpp vectorscan libdaq snort3 traffic-gen

.PHONY: dpdk
dpdk:
	@( \
	    cd ./components/dpdk; \
	    meson build -Dexamples=all; \
	    cd build; \
	    ninja -j $(NUM_JOBS); \
	)

.PHONY: vpp
vpp: libdaq
	@( \
	    cd ./components/vpp; \
	    git apply --whitespace=nowarn ../../patches/vpp/*.patch; \
	    export CMAKE_INCLUDE_PATH=${ROOT_DIR}/components/snort3/dependencies/libdaq/install/include:${CMAKE_INCLUDE_PATH}; \
	    $(MAKE) install-dep UNATTENDED=y; \
	    $(MAKE) build-release -j $(NUM_JOBS); \
	)


.PHONY: vectorscan
vectorscan:
	# Building & installing vector scan
	@( \
	    mkdir -p components/vectorscan/build; \
	    cd components/vectorscan/build; \
	    cmake -DCMAKE_INSTALL_PREFIX=`pwd`/../install ../ && $(MAKE) -j $(NUM_JOBS);\
	    $(MAKE) install; \
	)

.PHONY: libdaq
libdaq:
	# Building and installing libdaq
	@( \
	    cd components/snort3/dependencies/libdaq; \
	    ./bootstrap && ./configure --prefix=`pwd`/install; \
	    $(MAKE) -j $(NUM_JOBS) && $(MAKE) install; \
	)

.PHONY: snort3
snort3: libdaq vectorscan
	# Building and installing snort3
	@( \
	    cd components/snort3/; \
	    ./configure_cmake.sh --prefix=`pwd`/install --enable-tcmalloc --build-type=Release \
	    --with-daq-includes=`pwd`/dependencies/libdaq/install/include \
	    --with-daq-libraries=`pwd`/dependencies/libdaq/install/lib \
	    --with-hyperscan-includes=`pwd`/../vectorscan/install/include/hs \
	    --with-hyperscan-libraries=`pwd`/../vectorscan/install/lib \
	    --define=CMAKE_PREFIX_PATH:PATH=`pwd`/../vectorscan/install; \
	    cd build; \
	    $(MAKE) -j $(NUM_JOBS) && $(MAKE) install; \
	)

.PHONY: traffic-gen
traffic-gen:
	@( \
	    cd ./tools; \
	    $(MAKE) traffic-gen; \
	)

.PHONY: venv
venv: ${VENV_DIR}

${VENV_DIR}: doc/requirements.txt
	@( \
	    if [ ! -d ${VENV_DIR} ]; then \
	        ${PYTHON} -m venv ${VENV_DIR}; \
	    fi; \
	    . ${VENV_DIR}/bin/activate; \
	    ${PYTHON} -m pip install -r doc/requirements.txt; \
	    deactivate; \
	    touch ${VENV_DIR}; \
	)

.PHONY: venv-clean
venv-clean:
	@( \
	    rm -rf ${VENV_DIR}; \
	)

.PHONY: doc
doc: venv
	@( \
	    . ${VENV_DIR}/bin/activate; \
	    cd ./doc; \
	    $(MAKE) html; \
	    deactivate; \
	)

.PHONY: doc-clean
doc-clean: venv
	@( \
	    . ${VENV_DIR}/bin/activate; \
	    cd ./doc; \
	    $(MAKE) clean; \
	    deactivate; \
	    cd -; \
	    $(MAKE) venv-clean; \
	)

.PHONY: doc-spellcheck
doc-spellcheck: venv
	@( \
		. ${VENV_DIR}/bin/activate; \
		cd ./doc; \
		SPHINXOPTS=-W make spelling; \
	)

.PHONY: help
help:
	@echo "Make Targets:"
	@echo " all                  - Build all the components (except documentation)"
	@echo " doc                  - Build the Sphinx documentation"
	@echo " doc-clean            - Remove the generated files from the Sphinx docs"
	@echo " doc-spellcheck       - Spellcheck the Sphinx documentation"
	@echo " dpdk                 - Build DPDK"
	@echo " vpp                  - Build VPP"
	@echo " vectorscan           - Build & install vectorscan"
	@echo " libdaq               - Build & install libdaq"
	@echo " snort3               - Build & install snort3"
	@echo " traffic-gen          - Build traffic generator tools, e.g., wrk2"
