#!/usr/bin/env bash

# Copyright (c) 2023-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -eo pipefail

SUFFIX=$(whoami)
DIR=$(cd "$(dirname "$0")" || exit 1 ;pwd)

help_func() {
    echo "Usage: ./stop.sh options"
    echo
    echo "Options:"
    echo "  -s <suffix>  VPP directory name suffix (defaults to username)."
    echo "  -h           Show this message and quit."
    echo
}

options=(-o "hs:")
opts=$(getopt "${options[@]}" -- "$@")

eval set -- "$opts"

while true; do
    case "$1" in
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
          shift
          break
          ;;
      *)
          echo "Invalid Option!!"
          help_func
          exit 1
          ;;
    esac
done

echo "Stop VPP instance and iperf3 server..."

vpp_hs_pidfile="/run/vpp/${SUFFIX}/vpp_hs.pid"
iperf3_pidfile="/run/iperf3.pid"

if [ -f "${vpp_hs_pidfile}" ];then
    sudo kill -9 "$(cat "${vpp_hs_pidfile}")"
    sudo rm "${vpp_hs_pidfile}"
fi

if [ -f "${iperf3_pidfile}" ];then
    sudo kill -9 "$(sudo cat "${iperf3_pidfile}" | tr -d '\0')"
    sudo rm "${iperf3_pidfile}"
fi

sockfile="/run/vpp/${SUFFIX}/cli.sock"
vpp_hs_pidfile="/run/vpp/${SUFFIX}/vpp_hs.pid"

# renaming conf files back to original
old_conf_path="api-socket-name /run/vpp/${SUFFIX}/cli.sock"
new_conf_path="api-socket-name /run/vpp/cli.sock"

sed -i -e "s%$old_conf_path%$new_conf_path%g" ${DIR}/vcl_iperf3_client.conf ${DIR}/vcl_iperf3_server_lb.conf ${DIR}/vcl_iperf3_server_pn.conf
