#!/usr/bin/env bash

# Copyright (c) 2023-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

SUFFIX=$(whoami)

options=(-o "hs:")
opts=$(getopt "${options[@]}" -- "$@")
eval set -- "$opts"

help_func() {
    echo "Usage: ./stop.sh options"
    echo
    echo "Options:"
    echo "  -s <suffix>  VPP directory name suffix (defaults to username)."
    echo "  -h           Show this message and quit."
    echo
}

while true; do
    case "$1" in
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
          shift
          break
          ;;
      *)
          echo "Invalid Option!!"
          help_func
          exit 1
          ;;
    esac
done

echo "Stop VPP instances..."

VPP_LOCAL_PIDFILE="/run/vpp/local/${SUFFIX}/vpp_local.pid"
VPP_REMOTE_PIDFILE="/run/vpp/remote/${SUFFIX}/vpp_remote.pid"

if [[ -f "${VPP_LOCAL_PIDFILE}" ]];then
    sudo kill -9 "$(cat "${VPP_LOCAL_PIDFILE}")"
    sudo rm "${VPP_LOCAL_PIDFILE}"
fi

if [[ -f "${VPP_REMOTE_PIDFILE}" ]];then
    sudo kill -9 "$(cat "${VPP_REMOTE_PIDFILE}")"
    sudo rm "${VPP_REMOTE_PIDFILE}"
fi
