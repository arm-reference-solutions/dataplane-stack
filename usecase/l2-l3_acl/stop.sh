#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

SUFFIX=$(whoami)

help_func() {
    echo "Usage: ./stop.sh options"
    echo
    echo "Options:"
    echo "  -s <suffix>  VPP directory name suffix (defaults to username)."
    echo "  -h           Show this message and quit."
    echo
}

options=(-o "hs:")
opts=$(getopt "${options[@]}" -- "$@")

eval set -- "$opts"

while true; do
    case "$1" in
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
          shift
          break
          ;;
      *)
          echo "Invalid Option!!"
          help_func
          exit 1
          ;;
    esac
done

echo "Stop traffic and release VPP router & traffic generator instances..."

VPP_ACL_PIDFILE="/run/vpp/${SUFFIX}/acl/vpp_acl.pid"
VPP_TG_PIDFILE="/run/vpp/${SUFFIX}/tg/vpp_tg.pid"

if [[ -f "${VPP_ACL_PIDFILE}" ]];then
    sudo kill -9 "$(cat "${VPP_ACL_PIDFILE}")"
    sudo rm "${VPP_ACL_PIDFILE}"
fi

if [[ -f "${VPP_TG_PIDFILE}" ]];then
    sudo kill -9 "$(cat "${VPP_TG_PIDFILE}")"
    sudo rm "${VPP_TG_PIDFILE}"
fi
