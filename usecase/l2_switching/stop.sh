#!/usr/bin/env bash

# Copyright (c) 2022-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

SUFFIX=$(whoami)

options=(-o "hs:")
opts=$(getopt "${options[@]}" -- "$@")
eval set -- "$opts"

help_func() {
    echo "Usage: ./stop.sh options"
    echo
    echo "Options:"
    echo "  -s <suffix>  VPP directory name suffix (defaults to username)."
    echo "  -h           Show this message and quit."
    echo
}

while true; do
    case "$1" in
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
          shift
          break
          ;;
      *)
          echo "Invalid Option!!"
          help_func
          exit 1
          ;;
    esac
done

echo "Stop traffic and release switch & traffic_generator instances..."

vpp_sw_pidfile="/run/vpp/${SUFFIX}/sw/vpp_sw.pid"
vpp_tg_pidfile="/run/vpp/${SUFFIX}/tg/vpp_tg.pid"

if [ -f "${vpp_sw_pidfile}" ];then
    sudo kill -9 "$(cat "${vpp_sw_pidfile}")"
    sudo rm "${vpp_sw_pidfile}"
fi

if [ -f "${vpp_tg_pidfile}" ];then
    sudo kill -9 "$(cat "${vpp_tg_pidfile}")"
    sudo rm "${vpp_tg_pidfile}"
fi
