#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0


set -e

export vppctl_binary
export vpp_binary

DIR=$(dirname "$0")
DATAPLANE_TOP=${DIR}/../..
# shellcheck source=../../tools/check-path.sh
. "${DATAPLANE_TOP}"/tools/check-path.sh

SUFFIX=$(whoami)

help_func()
{
    echo "Usage: ./$(basename $0) options"
    echo
    echo "Options:"
    echo "  -c <core list>       set CPU affinity. Assign VPP main thread to 1st core"
    echo "                       in list and place worker threads on other listed cores."
    echo "                       Cores are separated by commas, and worker cores can"
    echo "                       include ranges."
    echo "  -s <suffix>          VPP directory name suffix (defaults to username)."
    echo "  -h                   show this message and quit"
    echo
    echo "Example:"
    echo "  ./$(basename $0) -c 1,3-4,6"
    echo
}

err_cleanup()
{
    echo "VPP router startup error, cleaning up..."
    if [[ -f "${VPP_PIDFILE}" ]]; then
        vpp_acl_pid=$(cat "${VPP_PIDFILE}")
        sudo kill -9 "${vpp_acl_pid}"
        sudo rm "${VPP_PIDFILE}"
    fi
    exit 1
}


cal_cores()
{
  IFS=',' read -ra array <<< "$1"

  count=0

  for item in "${array[@]}"; do
      if [[ $item == *-* ]]; then
          start=${item%-*}
          end=${item#*-}
          count=$((count + end - start + 1))
      else
          count=$((count + 1))
      fi
  done

  echo $count
}

setup_iface()
{
    sudo "${vppctl_binary}" -s "${SOCKFILE}" set int state Ethernet0 up
    sudo "${vppctl_binary}" -s "${SOCKFILE}" set int ip address Ethernet0 192.168.1.1/24
    sudo "${vppctl_binary}" -s "${SOCKFILE}" set int state Ethernet1 up
    sudo "${vppctl_binary}" -s "${SOCKFILE}" set int ip address Ethernet1 192.168.2.1/24
    sudo "${vppctl_binary}" -s "${SOCKFILE}" set ip neighbor Ethernet1 192.168.2.2 02:00:00:00:00:01
    sudo "${vppctl_binary}" -s "${SOCKFILE}" ip route add 0.0.0.0/0 via 192.168.2.2 Ethernet1

    LOG=$(sudo "${vppctl_binary}" -s "${SOCKFILE}" show interface)
    if ! [[ "${LOG}" == *Ethernet0* && "${LOG}" == *Ethernet1* ]]; then
        echo "Failed to set up interfaces!"
        err_cleanup
    fi


}

add_acl_rules()
{
    TCP_PROTO=6
    UDP_PROTO=17

    ACL_RULES=( 'permit+reflect src 198.18.125.2/32 dst 198.18.1.1/32 proto '${TCP_PROTO}' sport 500 dport 501' \
    'permit+reflect src 10.18.118.0/24 dst 10.18.1.0/24 proto '${UDP_PROTO}'' \
    'permit+reflect src 10.18.118.0/24 dst 10.18.10.0/24' \
    'deny src 10.18.0.0/16' \
    'deny src 10.0.0.0/8 dst 10.0.0.0/8' \
    'permit+reflect src 20.20.120.0/24 dst 20.120.10.0/24 proto '${UDP_PROTO}'' \
    'permit+reflect src 20.20.125.0/24 dst 20.120.11.0/24' \
    'deny src 20.0.0.0/8 dst 20.0.0.0/8' \
    'permit+reflect src 30.10.10.0/24 dst 30.120.10.0/24 proto '${UDP_PROTO}'' \
    'deny'  # Catch all : src 0.0.0.0/0 dst 0.0.0.0/0
    )

    for rule in "${ACL_RULES[@]}"; do
        sudo "${vppctl_binary}" -s "${SOCKFILE}" set acl-plugin acl "$rule"
    done
    for ((acl_index = 0; acl_index < ${#ACL_RULES[@]}; acl_index++)) do
        sudo "${vppctl_binary}" -s "${SOCKFILE}" set acl-plugin interface Ethernet0 input acl "$acl_index"
    done
}

options=(-o "hmp:c:f:s:")
opts=$(getopt "${options[@]}" -- "$@")
eval set -- "$opts"

while true; do
    case "$1" in
      -h)
          help_func
          exit 0
          ;;
      -c)
        if ! [[ "$2" =~ ^[0-9]{1,3}((,[0-9]{1,3})|(,[0-9]{1,3}-[0-9]{1,3}))+$ ]]; then
            echo "error: \"-c\" requires correct isolated cpu core id"
            help_func
            exit 1
        fi
        main_core=$(echo "$2" | cut -d "," -f 1)
        worker_cores=$(echo "$2" | cut -d "," -f 2-)
        if [[ "${main_core}" == "${worker_cores}" ]]; then
            echo "error: \"-c\" option bad usage"
            help_func
            exit 1
        fi
	queues_count=$(cal_cores "$worker_cores")
        shift 2
        ;;
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
          shift
          break
          ;;
      *)
          echo "Invalid Option!!"
          help_func
          exit 1
          ;;
    esac
done

VPP_RUNTIME_DIR="/run/vpp/${SUFFIX}/acl"
VPP_PIDFILE="${VPP_RUNTIME_DIR}/vpp_acl.pid"
SOCKFILE="${VPP_RUNTIME_DIR}/cli_acl.sock"
MEMIF_SOCKET1="/tmp/memif_dut_1-${SUFFIX}"
MEMIF_SOCKET2="/tmp/memif_dut_2-${SUFFIX}"

check_vppctl > /dev/null
check_vpp > /dev/null

echo "starting vpp"

sudo "${vpp_binary}" unix "{ runtime-dir ${VPP_RUNTIME_DIR} cli-listen ${SOCKFILE} pidfile ${VPP_PIDFILE} }"   \
                         cpu "{ main-core ${main_core} corelist-workers ${worker_cores} }"                         \
                         dpdk "{ no-pci single-file-segments dev default {num-tx-queues ${queues_count} num-rx-queues ${queues_count} }
                                 vdev net_memif0,role=client,id=1,socket-abstract=no,socket=${MEMIF_SOCKET1},mac=c6:ce:78:fe:5f:77,zero-copy=yes
                                 vdev net_memif1,role=client,id=1,socket-abstract=no,socket=${MEMIF_SOCKET2},mac=c6:ce:78:fe:5f:78,zero-copy=yes }"

echo "Running vpp"
sleep 0.5

set +e
max_conn_retries=10
for conn_count in $(seq ${max_conn_retries}); do
    if ! output=$(sudo "${vppctl_binary}" -s "${SOCKFILE}" show threads) ; then
        if [[ ${conn_count} -eq ${max_conn_retries} ]]; then
            err_cleanup
        fi
        sleep 0.5
    elif [[ -z "${output}" ]]; then
        err_cleanup
    else
        break
    fi
done

set -e

setup_iface # addding l3 fwd

echo "Adding ACL rules"

add_acl_rules

echo "VPP L3-L4 Firewall Ready"