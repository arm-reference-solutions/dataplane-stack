#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

export vppctl_binary
export vpp_binary

DIR=$(dirname "$0")
DATAPLANE_TOP=${DIR}/../..
# shellcheck source=../../tools/check-path.sh
. "${DATAPLANE_TOP}"/tools/check-path.sh
test_duration=3

SUFFIX=$(whoami)
help_func()
{
    echo "Usage: ./run_vpp_l2_acl_tg.sh options"
    echo
    echo "Options:"
    echo "  -t <duration>        time in seconds for testing (default 3 seconds)"
    echo "  -s <suffix>          VPP directory name suffix (defaults to username)."
    echo "  -h                   show this message and quit"
    echo
    echo "Example:"
    echo "  ./vpp_traffic_send.sh"
    echo
}

options=(-o "hs:")
opts=$(getopt "${options[@]}" -- "$@")

eval set -- "$opts"

while true; do
    case "$1" in
      -h)
        help_func
		exit 0
		;;
      -t)
        if ! [[ "$2" =~ ^[1-9][0-9]*$ ]]; then
            echo "error: \"-t\" requires correct test duration"
            help_func
            exit 1
        fi
		test_duration=$2
        shift 2
        ;;
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
        shift
        break
        ;;
      *)
        echo "Invalid Option!!"
        help_func
        exit 1
        ;;
    esac
done

VPP_ACL_RUNTIME_DIR="/run/vpp/${SUFFIX}/acl"
VPP_ACL_PIDFILE="${VPP_ACL_RUNTIME_DIR}/vpp_acl.pid"
ACL_SOCKFILE="${VPP_ACL_RUNTIME_DIR}/cli_acl.sock"
MEMIF_SOCKET1="/tmp/memif_dut_1-${SUFFIX}"
MEMIF_SOCKET2="/tmp/memif_dut_2-${SUFFIX}"

VPP_TG_RUNTIME_DIR="/run/vpp/${SUFFIX}/tg"
VPP_TG_PIDFILE="${VPP_TG_RUNTIME_DIR}/vpp_tg.pid"
TG_SOCKFILE="${VPP_TG_RUNTIME_DIR}/cli_tg.sock"

check_vppctl > /dev/null

sudo "${vppctl_binary}" -s "${TG_SOCKFILE}" show packet-generator
echo ""

packet_rules=(
"permit+reflect src 198.18.125.2/32 dst 198.18.1.1/32 proto 6 sport 500 dport 501"
"permit+reflect src 10.18.118.0/24 dst 10.18.1.0/24 proto UDP"
"permit+reflect src 10.18.118.0/24 dst 10.18.10.0/24"
"deny src 10.18.0.0/16"
"deny src 10.0.0.0/8 dst 10.0.0.0/8"
"permit+reflect src 20.20.120.0/24 dst 20.120.10.0/24 UDP"
"permit+reflect src 20.20.125.0/24 dst 20.120.11.0/24"
"deny src 20.0.0.0/8 dst 20.0.0.0/8" 
"permit+reflect src 30.10.10.0/24 dst 30.120.10.0/24 UDP"
"deny src 0.0.0.0/0 dst 0.0.0.0/0" 
)

rule_names=(
    "rule_0_permit" "rule_1_permit" "rule_2_permit" "rule_3_deny" "rule_4_deny" 
    "rule_5_permit" "rule_6_permit" "rule_7_deny" "rule_8_permit" "rule_9_deny"
)


for rule in "${!packet_rules[@]}";
do
    echo "${rule_names[rule]}"
    echo "${packet_rules[rule]}"
    echo ""
    sudo "${vppctl_binary}" -s  "${TG_SOCKFILE}" packet-generator disable-stream
    sleep 1
    sudo "${vppctl_binary}" -s "${ACL_SOCKFILE}" clear interfaces
    sudo "${vppctl_binary}" -s "${ACL_SOCKFILE}" clear trace
    sleep 1
    sudo "${vppctl_binary}" -s  "${TG_SOCKFILE}" packet-generator enable-stream ${rule_names[rule]}
    sudo "${vppctl_binary}" -s "${ACL_SOCKFILE}" trace add dpdk-input 1

	echo "Letting VPP route packets for ${test_duration} seconds:"
	for _ in $(seq "${test_duration}"); do
	    echo -n "..$_"
	    sleep 1
	done

    sudo "${vppctl_binary}" -s "${ACL_SOCKFILE}" show interface
    log=$(sudo "${vppctl_binary}" -s "${ACL_SOCKFILE}" show trace)
    echo ""
    echo ""
    echo "$log" | grep "acl"
    echo "================================================================================="
done
