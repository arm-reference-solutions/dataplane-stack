#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

export vppctl_binary
export vpp_binary

DIR=$(dirname "$0")
DATAPLANE_TOP=${DIR}/../..
# shellcheck source=../../tools/check-path.sh
. "${DATAPLANE_TOP}"/tools/check-path.sh

SUFFIX=$(whoami)

packet_len=60

src_mac="00:00:0a:81:00:01"
dst_mac="c6:ce:78:fe:5f:77"

help_func()
{
    echo "Usage: ./$(basename "$0") options"
    echo
    echo "Options:"
    echo "  -c <core list>       set CPU affinity. Assign VPP main thread to 1st core"
    echo "                       in list and place worker threads on other listed cores."
    echo "                       Cores are separated by commas, and worker cores can include"
    echo "                       ranges. The number of worker cores needs to be even."
    echo "  -l <length>          octet length of generated Ethernet packets, 60-2048. 60 by"
    echo "                       default. 4 octets frame check sequence (FCS) is not counted"
    echo "                       into the length, nor is it generated. Larger packets may reduce"
    echo "                       VPP traffic generator performance. Changing the packet length"
    echo "                       via this option should not affect the performance."
    echo "  -s <suffix>          VPP directory name suffix (defaults to username)."
    echo "  -h                   show this message and quit"
    echo
    echo "Example:"
    echo "  ./$(basename "$0") -c 1,3-4,6,8 -l 100"
    echo
}

err_cleanup()
{
    echo "VPP traffic generator startup error, cleaning up..."
    if [[ -f "${VPP_PIDFILE}" ]]; then
        vpp_tg_pid=$(cat "${VPP_PIDFILE}")
        sudo kill -9 "${vpp_tg_pid}"
        sudo rm "${VPP_PIDFILE}"
    fi
    exit 1
}

cal_cores()
{
  IFS=',' read -ra array <<< "$1"

  count=0

  for item in "${array[@]}"; do
      if [[ $item == *-* ]]; then
          start=${item%-*}
          end=${item#*-}
          count=$((count + end - start + 1))
      else
          count=$((count + 1))
      fi
  done

  echo ${count}
}

add_acl_streams()
{
    # Worker index = rule_number % queues_count

    data_arr=("IP4: ${src_mac} -> ${dst_mac}
                TCP: 198.18.125.2  -> 198.18.1.1
                TCP: 500 -> 501
                incrementing 8" #0

            "IP4: ${src_mac} -> ${dst_mac}
                UDP: 10.18.118.10  -> 10.18.1.10
                UDP: 1000 -> 1001
                incrementing 8" #1

            "IP4: ${src_mac} -> ${dst_mac}
                TCP: 10.18.118.10  -> 10.18.10.10
                TCP: 1002 -> 1003
                incrementing 8" #2

            "IP4: ${src_mac} -> ${dst_mac}
                TCP: 10.18.10.18  -> 20.18.1.10
                TCP: 1004 -> 1005
                incrementing 8" #3

            "IP4: ${src_mac} -> ${dst_mac}
                TCP: 10.1.0.18  -> 10.1.18.20
                TCP: 1006 -> 1007
                incrementing 8" #4

            "IP4: ${src_mac} -> ${dst_mac}
                UDP: 20.20.120.10  -> 20.120.10.10
                UDP: 1008 -> 1009
                incrementing 8" #5

            "IP4: ${src_mac} -> ${dst_mac}
                UDP: 20.20.125.15  -> 20.120.11.11
                UDP: 1008 -> 1009
                incrementing 8" #6

            "IP4: ${src_mac} -> ${dst_mac}
                UDP: 20.15.0.8  -> 20.12.0.5
                UDP: 1010 -> 1011
                incrementing 8" #7

            "IP4: ${src_mac} -> ${dst_mac}
                UDP: 30.10.10.1  -> 30.120.10.10
                UDP: 1010 -> 1011
                incrementing 8" #8

            "IP4: ${src_mac} -> ${dst_mac}
                UDP: 1.1.1.3  -> 1.1.1.2
                UDP: 1010 -> 1011
                incrementing 8" #9

    )

    rule_names=('rule_0_permit' 'rule_1_permit' 'rule_2_permit' 'rule_3_deny'  
    'rule_4_deny' 'rule_5_permit' 'rule_6_permit' 'rule_7_deny' 'rule_8_permit' 'rule_9_deny')

    for i in "${!data_arr[@]}"; do
        worker_index=$((i % workers_count))

        sudo "${vppctl_binary}" -s "${SOCKFILE}"  \
        "packet-generator new {
            name ${rule_names[i]}
            limit -1
            size ${packet_len}-${packet_len}
            worker ${worker_index}
            node memif1/1-output
            data {
                ${data_arr[i]}
            }
        }"
    done
}

options=(-o "hc:f:l:s:")
opts=$(getopt "${options[@]}" -- "$@")
eval set -- "$opts"

while true; do
    case "$1" in
      -h)
        help_func
        exit 0
        ;;
      -c)
        if ! [[ "$2" =~ ^[0-9]{1,3}((,[0-9]{1,3})|(,[0-9]{1,3}-[0-9]{1,3}))+$ ]]; then
            echo "error: \"-c\" requires correct isolated cpu core id"
            help_func
            exit 1
        fi
        main_core=$(echo "$2" | cut -d "," -f 1)
        worker_cores=$(echo "$2" | cut -d "," -f 2-)
        if [[ "${main_core}" == "${worker_cores}" ]]; then
            echo "error: \"-c\" requires different main core and worker core"
            help_func
            exit 1
        fi
        workers_count=$(cal_cores "$worker_cores")
        if (( workers_count % 2 != 0 )); then
            echo "error: \"-c\" requires an even number of worker cores"
            help_func
            exit 1
        fi
        queues_count=$((workers_count / 2))
        shift 2
        ;;
      -l)
        if ! [[ "$2" =~ ^[1-9][0-9]*$ ]]; then
            echo "error: \"-l\" requires octet length for generated packets"
            help_func
            exit 1
        fi
        if [[ "$2" -lt 60 ]] || [[ "$2" -gt 2048 ]]; then
            echo "error: \"-l\" requires a length between 60 and 2048."
            help_func
            exit 1
        fi
        packet_len=$2
        shift 2
        ;;
      -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
          SUFFIX="$2"
          shift 2
          ;;
      --)
        shift
        break
        ;;
      *)
        echo "Invalid Option!!"
        help_func
        exit 1
        ;;
    esac
done

VPP_RUNTIME_DIR="/run/vpp/${SUFFIX}/tg"
VPP_PIDFILE="${VPP_RUNTIME_DIR}/vpp_tg.pid"
SOCKFILE="${VPP_RUNTIME_DIR}/cli_tg.sock"
MEMIF_SOCKET1="/tmp/memif_dut_1-${SUFFIX}"
MEMIF_SOCKET2="/tmp/memif_dut_2-${SUFFIX}"

check_vpp > /dev/null
check_vppctl > /dev/null

sudo "${vpp_binary}" unix "{ runtime-dir ${VPP_RUNTIME_DIR} cli-listen ${SOCKFILE} pidfile ${VPP_PIDFILE} }"   \
                     cpu "{ main-core ${main_core} corelist-workers ${worker_cores} }"                         \
                     plugins "{ plugin default { disable } plugin memif_plugin.so { enable } }"
echo "VPP traffic generator starting up"
sleep 0.5

# Disable "Exit on Error" temporarily to allow vppctl to try connection several
# times for slow starting up VPP on some platforms.
set +e
max_conn_retries=10
for conn_count in $(seq ${max_conn_retries}); do
    if ! output=$(sudo "${vppctl_binary}" -s "${SOCKFILE}" show threads) ; then
        if [[ ${conn_count} -eq ${max_conn_retries} ]]; then
            err_cleanup
        fi
        sleep 0.5
    elif [[ -z "${output}" ]]; then
        err_cleanup
    else
        break
    fi
done

set -e

sudo "${vppctl_binary}" -s "${SOCKFILE}" create memif socket id 1 filename ${MEMIF_SOCKET1}
sudo "${vppctl_binary}" -s "${SOCKFILE}" create int memif id 1 socket-id 1 rx-queues "${queues_count}" tx-queues "${queues_count}" master
sudo "${vppctl_binary}" -s "${SOCKFILE}" create memif socket id 2 filename ${MEMIF_SOCKET2}
sudo "${vppctl_binary}" -s "${SOCKFILE}" create int memif id 1 socket-id 2 rx-queues "${queues_count}" tx-queues "${queues_count}" master
sudo "${vppctl_binary}" -s "${SOCKFILE}" set interface mac address memif1/1 02:fe:a4:26:ca:ac
sudo "${vppctl_binary}" -s "${SOCKFILE}" set interface mac address memif2/1 02:fe:51:75:42:ed
sudo "${vppctl_binary}" -s "${SOCKFILE}" set int state memif1/1 up
sudo "${vppctl_binary}" -s "${SOCKFILE}" set int state memif2/1 up

add_acl_streams

echo "TG packet streams are : "
echo $(sudo "${vppctl_binary}" -s "${SOCKFILE}" show packet-generator)

echo "TG is ready to send packets"