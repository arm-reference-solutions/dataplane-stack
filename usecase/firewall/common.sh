#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

export vpp_binary
export vppctl_binary

cal_cores() {
    IFS=',' read -ra array <<<"$1"

    count=0

    for item in "${array[@]}"; do
        if [[ $item == *-* ]]; then
            start=${item%-*}
            end=${item#*-}
            count=$((count + end - start + 1))
        else
            count=$((count + 1))
        fi
    done

    echo ${count}
}

check_cli() {
    # Disable "Exit on Error" temporarily to allow vppctl to try connection several
    # times for slow starting up VPP on some platforms.
    set +e
    max_conn_retries=10
    for conn_count in $(seq ${max_conn_retries}); do
        if ! output=$( (sudo "${VPPCTL}" -s "${1}" show threads) 2>&1); then
            if [[ ${conn_count} -eq ${max_conn_retries} ]]; then
                return 1
            fi
            sleep 0.5
        elif [[ -z "${output}" ]]; then
            return 1
        else
            break
        fi
    done
    set -e
    return 0
}

stop_snort() {
    # we want to delete dirs even if the pids are stale
    set +e
    if [[ -f "${SNORT_PIDFILE}" ]]; then
        echo "Stopping existing Snort instance with suffix ${SUFFIX}"
        sudo kill -9 "$(cat "${SNORT_PIDFILE}")"
        sudo rm ${SNORT_PIDFILE}
    fi
    set -e
}

stop_fw() {
    # we want to delete dirs even if the pids are stale
    set +e
    if [[ -f "${VPP_FW_PIDFILE}" ]]; then
        echo "Stopping existing VPP firewall with suffix ${SUFFIX}"
        sudo kill -9 "$(cat "${VPP_FW_PIDFILE}")"
        sudo rm -r ${VPP_FW_RUNTIME_DIR}
    fi
    set -e
}

stop_tg() {
    # we want to delete dirs even if the pids are stale
    set +e
    if [[ -f "${VPP_TG_PIDFILE}" ]]; then
        echo "Stopping existing VPP traffic generator with suffix ${SUFFIX}"
        sudo kill -9 "$(cat "${VPP_TG_PIDFILE}")"
        sudo rm -r ${VPP_TG_RUNTIME_DIR}
    fi
    set -e
}

send_to_tg() {
    sudo "${VPPCTL}" -s "${TG_SOCKFILE}" "$@"
}

send_to_fw() {
    sudo "${VPPCTL}" -s "${FW_SOCKFILE}" "$@"
}

DIR=$(dirname "$0")
DATAPLANE_TOP=$(readlink -m "${DIR}/../..")

. "${DIR}/get_community_rules.sh"
. "${DATAPLANE_TOP}"/tools/check-path.sh

SUFFIX=$(whoami)
SNORT_QUEUE_SZ=8192

# get binary paths
trap "echo 'Failed to find binaries, did you build all the components?'" EXIT
check_vpp >/dev/null
check_vppctl >/dev/null
check_snort >/dev/null
trap - EXIT
VPP=${vpp_binary}
VPPCTL=${vppctl_binary}
SNORT=${snort_binary}

PCAP_PATH=""
RULES_PATH=""

opts=$(getopt "${OPTIONS[@]}" -- "$@")
eval set -- "$opts"

while true; do
    case "$1" in
    -h)
        help_func
        exit 0
        ;;
    -p)
        PHY_IFACE="1"
        pcie_pattern='[0-9a-fA-F]{4}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}\.[0-9a-fA-F]'
        if ! [[ "$2" =~ ^${pcie_pattern},${pcie_pattern}$ ]]; then
            echo "Incorrect PCIe addresses format: $2"
            help_func
            exit 1
        fi
        PCIE_ADDR[0]=$(echo "$2" | cut -d "," -f 1)
        PCIE_ADDR[1]=$(echo "$2" | cut -d "," -f 2)
        if [[ "${PCIE_ADDR[0]}" == "${PCIE_ADDR[1]}" ]]; then
            echo "error: \"-p\" option bad usage"
            help_func
            exit 1
        fi
        shift 2
        ;;
    -c)
        if ! [[ "$2" =~ ^[0-9]{1,3}((,[0-9]{1,3})|(,[0-9]{1,3}-[0-9]{1,3}))+$ ]]; then
            echo "error: \"-c\" requires correct isolated cpu core id"
            help_func
            exit 1
        fi
        MAIN_CORE=$(echo "$2" | cut -d "," -f 1)
        WORKER_CORES=$(echo "$2" | cut -d "," -f 2-)
        if [[ "${MAIN_CORE}" == "${WORKER_CORES}" ]]; then
            echo "error: \"-c\" requires different main core and worker core"
            help_func
            exit 1
        fi
        WORKERS_COUNT=$(cal_cores "$WORKER_CORES")
        shift 2
        ;;

    -f)
        if [[ ! -f "$2" ]]; then
            echo "error: \"-f\" requires a path to an existing pcap file"
            help_func
            exit 1
        fi
        PCAP_PATH="$2"
        shift 2
        ;;
    -r)
        if [[ ! -f "$2" ]]; then
            echo "error: \"-r\" requires a path to existing rules file"
            help_func
            exit 1
        fi
        RULES_PATH="$2"
        shift 2
        ;;
    -q)
        if [[ ! $( $2>0 && ! ($2&($2-1)) ) ]]; then
            echo "error: \"-q\" requires a number that is a power of two"
            help_func
            exit 1
        fi
        SNORT_QUEUE_SZ="$2"
        shift 2
        ;;
    -s)
        if [[ -z "$2" ]]; then
            echo "error: \"-s\" requires suffix"
            help_func
            exit 1
        fi
        SUFFIX="$2"
        shift 2
        ;;
    -t)
        if ! [[ "$2" =~ ^[1-9][0-9]*$ ]]; then
            echo "error: \"-t\" requires correct test duration"
            help_func
            exit 1
        fi
        TEST_DURATION=$2
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "Invalid Option!!"
        help_func
        exit 1
        ;;
    esac
done

VPP_TG_RUNTIME_DIR="/run/vpp/${SUFFIX}/tg"
VPP_FW_RUNTIME_DIR="/run/vpp/${SUFFIX}/fw"
VPP_TG_PIDFILE="${VPP_TG_RUNTIME_DIR}/vpp.pid"
VPP_FW_PIDFILE="${VPP_FW_RUNTIME_DIR}/vpp.pid"
SNORT_PIDFILE="/run/vpp/${SUFFIX}/snort.pid"
TG_SOCKFILE="${VPP_TG_RUNTIME_DIR}/cli.sock"
FW_SOCKFILE="${VPP_FW_RUNTIME_DIR}/cli.sock"
MEMIF_SOCKET1="/tmp/memif_dut_1-${SUFFIX}"
MEMIF_SOCKET2="/tmp/memif_dut_2-${SUFFIX}"

sudo mkdir -p /run/vpp/${SUFFIX}
sudo chown $(whoami) /run/vpp/${SUFFIX}
