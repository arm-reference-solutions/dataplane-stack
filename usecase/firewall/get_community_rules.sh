#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

DIR=$(dirname "$0")

# get community rules if missing
if [[ ! -d "${DIR}/snort3-community-rules" ]]; then
    echo "Downloading snort community rules"
    pushd ${DIR} > /dev/null
    wget -q https://www.snort.org/downloads/community/snort3-community-rules.tar.gz -O - | tar -xzf -
    echo "Snort community rules are here:"
    echo "${DIR}/snort3-community-rules/snort3-community.rules"
    popd > /dev/null
fi
