#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

OPTIONS=(-o "hc:f:s:")

help_func() {
    echo "Usage: ./run_vpp_tg.sh options"
    echo
    echo "Options:"
    echo "  -c <core list>  Set CPU affinity. Assign VPP main thread to 1st core"
    echo "                  in list and place worker threads on other listed cores."
    echo "                  Cores are separated by commas, and worker cores can include"
    echo "                  ranges. The number of worker cores needs to be even."
    echo "  -f <pcap path>  Instead of generating simple packets, playback the pcap."
    echo "                  Each worker will play back the pcap."
    echo "  -s <suffix>     VPP directory name suffix (defaults to username)."
    echo "  -h              Show this message and quit."
    echo
    echo "Example:"
    echo "  ./run_vpp_tg.sh -c 1,3-4,6,8"
    echo
}

start_vpp() {
    sudo "${VPP}" \
        "unix {
            runtime-dir ${VPP_TG_RUNTIME_DIR}
            cli-listen ${TG_SOCKFILE}
            pidfile ${VPP_TG_PIDFILE}
        }
        cpu {
            main-core ${MAIN_CORE}
            corelist-workers ${WORKER_CORES}
        }
        plugins {
            plugin default { disable }
            plugin memif_plugin.so { enable }
        }"
}

setup_iface() {
    send_to_tg create memif socket id 1 filename ${MEMIF_SOCKET1}
    send_to_tg create int memif id 1 socket-id 1 rx-queues ${queues_count} \
        tx-queues ${queues_count} master
    send_to_tg create memif socket id 2 filename ${MEMIF_SOCKET2}
    send_to_tg create int memif id 1 socket-id 2 rx-queues ${queues_count} \
        tx-queues ${queues_count} master
    send_to_tg set interface mac address memif1/1 02:fe:a4:26:ca:ac
    send_to_tg set interface mac address memif2/1 02:fe:51:75:42:ed
    send_to_tg set int state memif1/1 up
    send_to_tg set int state memif2/1 up
}

setup_tg() {
    if [ -z "$PCAP_PATH" ]; then
        echo "Generating synthetic traffic (please use example.rules in snort)"
        for ((index = 0; index < queues_count; index++)); do
            send_to_tg \
                "packet-generator new {
                    name tg-detect-${index}
                    limit -1
                    size 64-64
                    rate 1
                    worker ${index}
                    node memif1/1-output
                    data {
                        IP4: 00:00:0a:81:00:01 -> c6:ce:78:fe:5f:77
                        UDP: 200.0.0.1  -> 1.0.0.1
                        UDP: 1234 -> 2345
                        incrementing 8
                    }
                }"
            send_to_tg \
                "packet-generator new {
                    name tg-benign-${index}
                    limit -1
                    size 64-64
                    rate 1000
                    worker ${index}
                    node memif1/1-output
                    data {
                        IP4: 00:00:0a:81:00:01 -> c6:ce:78:fe:5f:77
                        UDP: 200.0.0.1  -> 1.0.0.1
                        UDP: 1234 -> 2345
                        incrementing 2
                    }
                }"
        done
    else
        echo "Using pcap ${PCAP_PATH} to generate traffic"
        for ((index = 0; index < queues_count; index++)); do
            send_to_tg create packet-generator interface pg${index}
            send_to_tg packet-generator new pcap "$PCAP_PATH" source \
                pg${index} name tg${index} \
                rate 1000 node memif1/1-output
            send_to_tg packet-generator configure tg0 limit 0
        done
    fi

    send_to_tg packet-generator enable-stream

    log=$(send_to_tg show packet-generator)

    if [[ ! "${log}" == *Yes* ]]; then
        echo "Failed to start VPP traffic generator"
        stop_tg
        exit 1
    fi
}

. $(dirname "$0")/common.sh

if [[ $((WORKERS_COUNT % 2)) -ne 0 ]]; then
    echo "error: \"-c\" requires an even number of worker cores"
    help_func
    exit 1
fi
queues_count=$((WORKERS_COUNT / 2))

# in case one is already running
stop_tg

echo "Launching VPP"

start_vpp
check_cli ${TG_SOCKFILE} || $(stop_fw && echo "Failed to launch VPP" && exit 1)

echo "Setting up interfaces"

setup_iface

echo "Setting up traffic generator"

setup_tg

echo "Successfully started VPP with traffic generator"
echo
