#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

OPTIONS=(-o "hs:")

help_func() {
    echo "Usage: ./stop.sh options"
    echo
    echo "Options:"
    echo "  -s <suffix>  VPP directory name suffix (defaults to username)."
    echo "  -h           Show this message and quit."
    echo
}

. $(dirname "$0")/common.sh

stop_snort
stop_fw
stop_tg

sudo rm  -rf /run/vpp/${SUFFIX}

echo "All instances with suffix ${SUFFIX} stopped"
