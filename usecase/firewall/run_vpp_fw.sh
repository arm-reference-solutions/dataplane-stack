#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

OPTIONS=(-o "hp:c:s:r:q:")

help_func() {
    echo "Usage: ./run_vpp_fw.sh options"
    echo
    echo "Options:"
    echo "  -c <core list>       Set CPU affinity. Assign VPP main thread to 1st core"
    echo "                       in list and place worker threads on other listed cores."
    echo "                       Cores are separated by commas, and worker cores can"
    echo "                       include ranges."
    echo "  -p <PCIe addresses>  Test via DPDK physical NIC. Require input NIC port PCIe"
    echo "                       address followed by output NIC port PCIe address,"
    echo "                       separated by comma."
    echo "  -r <path>            Rules for snort to run (defaults to example.rules"
    echo "                       unless a pcap is used in which case it uses community rules)."
    echo "  -q <bytes>           Size of queue used by the Snort plugin in VPP (defaults to 8192)."
    echo "  -s <suffix>          VPP directory name suffix (defaults to username)."
    echo "  -h                   Show this message and quit."
    echo
    echo "Example:"
    echo "  ./run_vpp_fw.sh -c 1,3-4,6"
    echo "  ./run_vpp_fw.sh -p 0001:01:00.0,0001:01:00.1 -c 1,3-4,6"
    echo
}

setup_iface() {
    send_to_fw set int ip address Ethernet0 192.168.1.1/24
    send_to_fw set int ip address Ethernet1 192.168.2.1/24
    send_to_fw set ip neighbor Ethernet1 192.168.2.2 02:00:00:00:00:01
    send_to_fw ip route add 0.0.0.0/0 via 192.168.2.2 Ethernet1

    echo "Adding snort instance"
    send_to_fw snort create-instance name snort1 queue-size ${SNORT_QUEUE_SZ} \
        on-disconnect pass
    echo "Attaching interfaces to snort"
    sleep 0.5
    send_to_fw snort attach instance snort1 interface Ethernet0 inout
    sleep 0.5
    send_to_fw snort attach instance snort1 interface Ethernet1 inout
    sleep 0.5

    send_to_fw set int state Ethernet0 up
    send_to_fw set int state Ethernet1 up

    LOG=$(send_to_fw show snort interfaces)
    if ! [[ "${LOG}" == *Ethernet0* && "${LOG}" == *Ethernet1* ]]; then
        echo "Failed to attach snort interfaces!"
        return 1
    fi
    return 0
}

start_vpp() {
    if [[ -n "$PHY_IFACE" ]]; then
        sudo "${VPP}" \
            "unix {
                runtime-dir ${VPP_FW_RUNTIME_DIR}
                cli-listen ${FW_SOCKFILE}
                pidfile ${VPP_FW_PIDFILE}
            }
            cpu {
                main-core ${MAIN_CORE}
                corelist-workers ${WORKER_CORES}
            }
            plugins {
                plugin default { disable }
                plugin dpdk_plugin.so { enable }
                plugin snort_plugin.so { enable }
            }
            dpdk {
                dev default {
                    num-tx-queues ${queues_count}
                    num-rx-queues ${queues_count}
                }
                dev ${PCIE_ADDR[0]} { name Ethernet0 }
                dev ${PCIE_ADDR[1]} { name Ethernet1 }
            }"
    else
        sudo "${VPP}" \
            "unix {
                runtime-dir ${VPP_FW_RUNTIME_DIR}
                cli-listen ${FW_SOCKFILE}
                pidfile ${VPP_FW_PIDFILE}
            }
            cpu {
                main-core ${MAIN_CORE}
                corelist-workers ${WORKER_CORES}
            }
            plugins {
                plugin default { disable }
                plugin dpdk_plugin.so { enable }
                plugin snort_plugin.so { enable }
            }
            dpdk {
                no-pci single-file-segments
                dev default {
                    num-tx-queues ${queues_count}
                    num-rx-queues ${queues_count}
                }
                vdev net_memif0,role=client,id=1,socket-abstract=no,socket=${MEMIF_SOCKET1},mac=c6:ce:78:fe:5f:77,zero-copy=yes
                vdev net_memif1,role=client,id=1,socket-abstract=no,socket=${MEMIF_SOCKET2},mac=c6:ce:78:fe:5f:78,zero-copy=yes
            }"
    fi
}

start_snort() {
    alert_format="alert_csv"
    alert_config="{file = true, fields = \"action pkt_num gid sid msg src_addr src_port dst_addr dst_port\"}"
    log_dir="${DATAPLANE_TOP}/log"
    if [ -d $log_dir ]
    then
        echo "Clearing old snort logs"
        rm -rf $log_dir
    fi
    mkdir -p $log_dir

    daq_dir="${DATAPLANE_TOP}/components/vpp/build-root/install-vpp-native/vpp/lib/aarch64-linux-gnu/daq"

    setsid -w sudo LD_LIBRARY_PATH=${DATAPLANE_TOP}/components/snort3/dependencies/libdaq/install/lib:${LD_LIBRARY_PATH} $SNORT \
            -c "${DATAPLANE_TOP}/components/snort3/install/etc/snort/snort.lua" \
            -R "${RULES_PATH}" --lua "search_engine.search_method = 'hyperscan'" \
            --snaplen 0 --daq-dir=${daq_dir} --daq vpp \
            --daq-var socket_name=${VPP_FW_RUNTIME_DIR}/snort.sock --daq-var debug \
            -i snort1 -k none -Q --warn-conf-strict -l ${log_dir} -d -v -e \
            --lua "${alert_format} = ${alert_config}" --create-pidfile \
            > "${log_dir}/startup_log" 2>&1 &

    while [ ! -f ${log_dir}/snort.pid ]; do 
	echo Waiting for snort.pid file
	sleep 1
        (( ct+=1 ))
        if [ $ct -gt 10 ]; then
            echo snort.pid did not become available, stop may need manual cleanup.
            break;
        fi
    done
    snort_pid=$(sudo cat ${log_dir}/snort.pid)
    echo $snort_pid >$SNORT_PIDFILE
}

. $(dirname "$0")/common.sh

queues_count=$WORKERS_COUNT

# set default RULES_PATH based on what the traffic generator is sending
if [[ -z "${RULES_PATH}" ]]; then
    # use community rules by default
    RULES_PATH="${DIR}/snort3-community-rules/snort3-community.rules"

    # if there's a generator stream called "benign"
    # use special example rules that match the generated traffic
    if [[ -f "${VPP_TG_PIDFILE}" ]]; then
        log=$(send_to_tg show packet-generator)
        if [[ "${log}" == *benign* ]]; then
            RULES_PATH="${DIR}/example.rules"
        fi
    fi
fi

# in case one is already running
stop_fw

echo "Launching VPP"

start_vpp
check_cli ${FW_SOCKFILE} || $(stop_fw && echo "Failed to launch VPP" && exit 1)

echo "Setting up interfaces and routes"

setup_iface || $(stop_fw && echo "Failed to setup interfaces" && exit 1)

echo "Successfully started VPP firewall"
echo

# in case one is already running
stop_snort

echo "Starting snort instance"

start_snort

echo "Snort started, check ${log_dir} for alerts:"
echo "$ sudo tail -f ${log_dir}/alert_csv.txt"
