#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

OPTIONS=(-o "hs:t:")

help_func() {
    echo "Usage: ./traffic_monitor.sh options"
    echo
    echo "Options:"
    echo "  -t <duration>  Time in seconds for testing (default 3 seconds)."
    echo "  -s <suffix>    VPP directory name suffix (defaults to username)."
    echo "  -h             Show this message and quit."
    echo
}

TEST_DURATION=3

. $(dirname "$0")/common.sh

send_to_fw "clear interfaces"
send_to_fw "clear run"

echo "Letting VPP route packets for ${TEST_DURATION} seconds:"

for _ in $(seq "${TEST_DURATION}"); do
    echo -n "..$_"
    sleep 1
done

echo
echo
echo "=========="

send_to_fw "show interface"
send_to_fw "show run"
