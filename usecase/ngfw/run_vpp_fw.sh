#!/usr/bin/env bash

# Copyright (c) 2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -e

OPTIONS=(-o "hp:c:s:r:q:d:a:")

help_func() {
    echo "Usage: ./run_vpp_fw.sh options"
    echo
    echo "Options:"
    echo "  -c <core list>       Set CPU affinity. Assign VPP main thread to 1st core"
    echo "                       in list and place worker threads on other listed cores."
    echo "                       Cores are separated by commas, and worker cores can"
    echo "                       include ranges. Workers should not overlap with Snort"
    echo "                       workers, but main may overlap Snort main."
    echo "  -d <core list>       Set CPU affinity of Snort. Assign Snort main thread to"
    echo "                       1st core and place snort worker threads on other listed"
    echo "                       cores. Cores separated by commas, and worker cores can"
    echo "                       include ranges. Workers should not overlap with VPP"
    echo "                       workers, but main may overlap VPP main."
    echo "  -a <core list>       Enable async crypto on list of VPP worker cores. Cores can be"
    echo "                       separated by commas, or include ranges. Core 0 in this list"
    echo "                       is the first worker core in the -c <core list> option."
    echo "  -p <PCIe addresses>  Test via DPDK physical NIC. Require input NIC port PCIe"
    echo "                       address followed by output NIC port PCIe address,"
    echo "                       separated by comma."
    echo "  -r <path>            Rules for snort to run (defaults to example.rules"
    echo "                       unless a pcap is used in which case it uses community rules)."
    echo "  -q <bytes>           Size of queue used by the Snort plugin in VPP (defaults to 8192)."
    echo "  -s <suffix>          VPP directory name suffix (defaults to username)."
    echo "  -h                   Show this message and quit."
    echo
    echo "Example:"
    echo "  ./run_vpp_fw.sh -c 1,3-4,6"
    echo "  ./run_vpp_fw.sh -c 101,104-106,109-110 -d 101,107-108 -a 3-4"
    echo "  ./run_vpp_fw.sh -p 0001:01:00.0,0001:01:00.1 -c 1,3-4,6"
    echo
}

setup_ipsec()
{
    send_to_fw create ipip tunnel src 192.168.2.1 dst 192.168.2.2
    send_to_fw ipsec sa add 10 spi 100 crypto-key 4a506a794f574265564551694d653768 crypto-alg aes-gcm-128
    send_to_fw ipsec sa add 20 spi 200 crypto-key 4a506a794f574265564551694d653768 crypto-alg aes-gcm-128
    send_to_fw ipsec tunnel protect ipip0 sa-in 20 sa-out 10
    send_to_fw set int state ipip0 up
    send_to_fw set int unnumbered ipip0 use Ethernet1

    if [ $IPSEC_WORKERS_COUNT -ne 0 ]; then
        send_to_fw set ipsec async mode on
        send_to_fw set crypto async dispatch mode polling
        # disable crypto on all workers
        for ((i=0;i<$WORKERS_COUNT;i++)); do
            send_to_fw set sw_scheduler worker $i crypto off
        done
        # enable crypto only on selected workers
        for ((i=0;i<$IPSEC_WORKERS_COUNT;i++)); do
            send_to_fw set sw_scheduler worker ${IPSEC_WORKER_CORES[i]} crypto on 
        done
    fi
}

add_acl_rules()
{
# l2 acls
    send_to_fw set acl-plugin macip acl permit ip 10.0.0.0/8 mac 00:22:33:44:55:66 mask ff:ff:ff:ff:ff:ff, \
                                            deny ip 10.0.0.0/8 mac 02:22:44:45:56:8a mask ff:ff:ff:ff:00:00, \
                                            deny 

    send_to_fw set acl-plugin macip interface Ethernet0 add acl 0

# l3 acls
	TCP_PROTO=6
	send_to_fw set acl-plugin acl deny src 10.0.0.5/32 dst 10.1.0.5/32 proto ${TCP_PROTO} sport 500 dport 501, \
									allow
	send_to_fw set acl-plugin interface Ethernet0 input acl 0

        ESP_PROTO=50
        send_to_fw set acl-plugin acl permit src 192.168.2.2/32 dst 192.168.2.1/32 proto ${ESP_PROTO}, \
                   deny
        send_to_fw set acl-plugin interface Ethernet1 input acl 1
}

setup_routing() {
    send_to_fw set ip neighbor Ethernet1 192.168.2.2 02:fe:51:75:42:ed 

    send_to_fw ip route add 10.0.0.0/8 via ipip0
    send_to_fw ip route add 11.0.0.0/8 via 192.168.1.2 Ethernet0
    send_to_fw set ip neighbor Ethernet0 192.168.1.2 02:fe:a4:26:ca:ac 
}

setup_snort() {
    echo "Adding snort instance"
    send_to_fw snort create-instance name snort1 queue-size ${SNORT_QUEUE_SZ} \
        on-disconnect pass
    echo "Attaching interfaces to snort"
    sleep 0.5
    send_to_fw snort attach instance snort1 interface Ethernet0 inout
    sleep 0.5
    send_to_fw snort attach instance snort1 interface Ethernet1 inout
    sleep 0.5

    LOG=$(send_to_fw show snort interfaces)
    if ! [[ "${LOG}" == *Ethernet0* && "${LOG}" == *Ethernet1* ]]; then
        echo "Failed to attach snort interfaces!"
        return 1
    fi
}

setup_iface() {
    send_to_fw set int ip address Ethernet0 192.168.1.1/24
    send_to_fw set int ip address Ethernet1 192.168.2.1/24

    setup_snort

    send_to_fw set int state Ethernet0 up
    send_to_fw set int state Ethernet1 up

    return 0
}

start_vpp() {
    if [[ -n "$PHY_IFACE" ]]; then
        sudo "${VPP}" \
            "unix {
                runtime-dir ${VPP_FW_RUNTIME_DIR}
                cli-listen ${FW_SOCKFILE}
                pidfile ${VPP_FW_PIDFILE}
            }
            cpu {
                main-core ${MAIN_CORE}
                corelist-workers ${WORKER_CORES}
            }
            plugins {
                plugin default { disable }
                plugin dpdk_plugin.so { enable }
                plugin snort_plugin.so { enable }
                plugin acl_plugin.so { enable }
                plugin crypto_native_plugin.so {enable} 
                plugin crypto_openssl_plugin.so {enable}
                plugin crypto_sw_scheduler_plugin.so {enable}
            }
            dpdk {
                dev default {
                    num-tx-queues ${queues_count}
                    num-rx-queues ${queues_count}
                }
                dev ${PCIE_ADDR[0]} { name Ethernet0 }
                dev ${PCIE_ADDR[1]} { name Ethernet1 }
            }"
    else
        sudo "${VPP}" \
            "unix {
                runtime-dir ${VPP_FW_RUNTIME_DIR}
                cli-listen ${FW_SOCKFILE}
                pidfile ${VPP_FW_PIDFILE}
            }
            cpu {
                main-core ${MAIN_CORE}
                corelist-workers ${WORKER_CORES}
            }
            plugins {
                plugin default { disable }
                plugin dpdk_plugin.so { enable }
                plugin snort_plugin.so { enable }
                plugin acl_plugin.so { enable }
                plugin crypto_native_plugin.so { enable} 
                plugin crypto_openssl_plugin.so { enable}
                plugin crypto_sw_scheduler_plugin.so { enable}
            }
            dpdk {
                no-pci single-file-segments
                dev default {
                    num-tx-queues ${queues_count}
                    num-rx-queues ${queues_count}
                }
                vdev net_memif0,role=client,id=1,socket-abstract=no,socket=${MEMIF_SOCKET1},mac=c6:ce:78:fe:5f:77,zero-copy=yes
                vdev net_memif1,role=client,id=1,socket-abstract=no,socket=${MEMIF_SOCKET2},mac=c6:ce:78:fe:5f:78,zero-copy=yes
            }"
    fi
}

start_snort() {
    alert_format="alert_csv"
    alert_config="{file = true, fields = \"action pkt_num gid sid msg src_addr src_port dst_addr dst_port\"}"
    log_dir="${DATAPLANE_TOP}/log"
    if [ -d $log_dir ]
    then
        echo "Clearing old snort logs"
        rm -rf $log_dir
    fi
    mkdir -p $log_dir

    daq_dir="${DATAPLANE_TOP}/components/vpp/build-root/install-vpp-native/vpp/lib/aarch64-linux-gnu/daq"

# create config for cpu pins for snort
   thread_cfg=""
   mp=""
   if [[ $SNORT_WORKERS_COUNT -ne 0 ]]; then
        mp="--max-packet-threads=${SNORT_WORKERS_COUNT}"
        thread_cfg="threads = {"
        thread_cfg+="{ type = 'main', cpuset = '$SNORT_MAIN_CORE' },"
        thread_cfg+="{ type = 'other', cpuset= '$SNORT_MAIN_CORE' },"
        for ((i=0;i<$SNORT_WORKERS_COUNT;i++)); do
            thread_cfg+="{ thread = $i, cpuset= '${SNORT_WORKER_CORES[$i]}' },"
        done
        thread_cfg+="}"
        thread_cfg+="process = { threads = threads }"
        thread_cfg+="snort_whitelist_append(\"threads\")"

   fi

   setsid -w sudo LD_LIBRARY_PATH=${DATAPLANE_TOP}/components/snort3/dependencies/libdaq/install/lib:${LD_LIBRARY_PATH} $SNORT \
            -c "${DATAPLANE_TOP}/components/snort3/install/etc/snort/snort.lua" \
            -R "${RULES_PATH}" --lua "search_engine.search_method = 'hyperscan' \
            ${alert_format} = ${alert_config} \
            ${thread_cfg}" \
            ${mp} \
            --snaplen 0 --daq-dir=${daq_dir} --daq vpp \
            --daq-var socket_name=${VPP_FW_RUNTIME_DIR}/snort.sock --daq-var debug \
            -i snort1 -k none -Q --warn-conf-strict -l ${log_dir} -d -v -e \
            --create-pidfile \
            > "${log_dir}/startup_log" 2>&1 &

    while [ ! -f ${log_dir}/snort.pid ]; do 
        echo Waiting for snort.pid file
        sleep 1
        (( ct+=1 ))
        if [ $ct -gt 10 ]; then
            echo snort.pid did not become available, stop may need manual cleanup.
            break;
        fi
    done
    snort_pid=$(sudo cat ${log_dir}/snort.pid)
    echo $snort_pid >$SNORT_PIDFILE
}

SNORT_WORKERS_COUNT=0
IPSEC_WORKERS_COUNT=0

. $(dirname "$0")/common.sh

queues_count=$WORKERS_COUNT

# set default RULES_PATH based on what the traffic generator is sending
if [[ -z "${RULES_PATH}" ]]; then
    # use community rules by default
    RULES_PATH="${DIR}/snort3-community-rules/snort3-community.rules"

    # if there's a generator stream called "benign"
    # use special example rules that match the generated traffic
    if [[ -f "${VPP_TG_PIDFILE}" ]]; then
        log=$(send_to_tg show packet-generator)
        if [[ "${log}" == *benign* ]]; then
            RULES_PATH="${DIR}/example.rules"
        fi
    fi
fi

# in case one is already running
stop_fw

echo "Launching VPP"

start_vpp

check_cli ${FW_SOCKFILE} || $(stop_fw && echo "Failed to launch VPP" && exit 1)

echo "Setting up interfaces"

setup_iface || $(stop_fw && echo "Failed to setup interfaces" && exit 1)

echo "Setting up acls"

add_acl_rules

echo "Setting up IPSec"

setup_ipsec

echo "Setting up routes"

setup_routing

echo "Successfully started VPP firewall"
echo

# in case one is already running
stop_snort

echo "Starting snort instance"

start_snort

echo "Snort started, check ${log_dir} for alerts:"
echo "$ sudo tail -f ${log_dir}/alert_csv.txt"


